//
//  GlobalConstant.swift
//  Iroid
//
//  Created by iroid on 30/03/18.
//  Copyright © 2018 iroidiroid. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


var currentLatitude = "21.216820"
var currentLongitude = "72.830269"
let POST_METHOD_CALL = true
let GET_METHOD_CALL = false
var uesrRole = Int()




let ACCESS_KEY                           =      "Bearer"
let DEVICE_UNIQUE_IDETIFICATION : String = UIDevice.current.identifierForVendor!.uuidString
let APPLICATION_NAME                     =      "Val-A"
let APLLICATION_JSON                     =      "application/json"
let USER_DETAILS                         =      "user_details"
let IS_LOGIN                             =      "is_login"
let IS_GUEST                             =      "is_guest"

let IS_ADD_SOCAIL_LINK                   =      "is_add_social_Link"
let SIGN_UP_STEP                         =      "signup_step"
let teamOfUseLink                        =      "https://val-a.com/terms"
let privacyLink                          =      "https://val-a.com/privacy"
/* Google Client id */

//
let STRIP_MODE               =              "staging"
//let STRIP_MODE                 =             "production"

let FULL_NAME                  =             "full_name"
let USER_NAME                  =             "username"
let DATE_OF_BIRTH              =             "date_of_birth"
let COMPANY_NAME               =             "companyName"
let TITLE                      =             "title"
//let EMAIL                    =             "email"
let PHONE_NUMBER               =             "phone_number"
let GENDER                     =             "gender"
let BIRTHDATE                  =             "birthdate"
let PASSWORD                   =             "password"
let PASSWORD_CONFIRM           =             "password_confirmation"
let ZIP_CODE                   =             "zip_code"
let BIO                        =             "bio"
let PROFILE_PICTURE            =             "profilePicture"
let USER_ID                    =             "user_id"
let TYPE                       =             "type"
let POST_ID                    =             "post_id"
let ACCESS_TOKEN               =             "access_token"
let REFRESH_TOKEN              =             "refreshToken"
let AUTHENTICATION             =             "authentication"
let PROFILE_DATA               =             "ProfileData"
let OPERATION_TYPE             =             "operationType"
let VERIFICATION_CODE          =             "verification_code"
let COMMENT                    =             "comment"
let MENTION_USER               =             "mention_users"
let DESCRIPTION                =             "description"
let THUMBNAIL                  =             "thumbnail"
let EMAIL_ADDRESS              =             "emailAddress"
let ADDRESS                    =             "address"
let COMMAND                    =             "command"
let TAG                        =             "tag"
let OLD_PASSWORD               =             "oldPassword"
let CONFIRM_PASSWORD           =            "password_confirmation"
let USER                       =            "user"
let ACCOUNT_USERNAME           =             "accountUsername"
let SOCIAL_PROVIDER            =             "socialProvider"
let SOCIAL_PROVIDER_TYPE       =             "socialProviderType"
let IS_FROM_PRODUCT            =              0
let IS_FROM_BRAND              =              1
let IS_FROM_DISPENSARY         =              2
let IS_FROM_MEDIA              =              0
let IS_FROM_YOUTUBE            =              1


enum OtpScreenNavigationType : Int{
    case whoAreYou = 1
    case forgotPassword = 2
    case afterLogin = 3
    case fromHome = 4
    case editProfile = 5
}
