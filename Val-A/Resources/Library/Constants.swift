//
//  Constants.swift
//  Medics2you
//
//  Created by Techwin iMac-2 on 11/03/20.
//  Copyright © 2020 Techwin iMac-2. All rights reserved.
//

import Foundation
import UIKit

let APP_NAME = "Val-A"

struct STORYBOARD {
    
    static let onBoarding = UIStoryboard(name: "OnBoarding", bundle: Bundle.main)
    static let menu = UIStoryboard(name: "Menu", bundle: Bundle.main)
    static let setting = UIStoryboard(name: "Setting", bundle: Bundle.main)
    static let login = UIStoryboard(name: "Login", bundle: Bundle.main)
    static let changePassword = UIStoryboard(name: "ChangePassword", bundle: Bundle.main)
    static let register = UIStoryboard(name: "Register", bundle: Bundle.main)
    static let forgot = UIStoryboard(name: "Forgot", bundle: Bundle.main)
    static let payForParkDescription = UIStoryboard(name: "payForParkDescription", bundle: nil)
    static let auction = UIStoryboard(name: "Auction", bundle: Bundle.main)
    static let profile = UIStoryboard(name: "Profile", bundle: nil)
    static let tabbar = UIStoryboard(name: "TabBar", bundle: Bundle.main)
    static let editProfile = UIStoryboard(name: "EditProfile", bundle: Bundle.main)
    static let otp = UIStoryboard(name: "Otp", bundle: Bundle.main)
    static let webView = UIStoryboard(name: "WebView", bundle: Bundle.main)
    static let activePluto = UIStoryboard(name: "ActivePluto", bundle: Bundle.main)
    static let payForPark = UIStoryboard(name: "PayForPark", bundle: Bundle.main)
    static let search = UIStoryboard(name: "Search", bundle: Bundle.main)
    static let onBoard = UIStoryboard(name: "OnBoard", bundle: Bundle.main)

}

//Production URL
//let server_url = "https://api.val-a.com/api/v1/"
//Development URL New
let server_url = "https://api.val-a.com/dev-parking1/api/v1/"
let loginURL = server_url+"login"
let vehicleMakeURL = server_url+"vehicle-make"
let vehicleModelURL = server_url+"vehicle-model"
let vehicleYearURL = server_url+"vehicle-year"
let vehicleColorURL = server_url+"vehicle-color"
let zoneListURL = server_url+"get-zone-list"
let parkingTimeDuration = server_url+"parking-time-duration"
let registerURL = server_url+"register"
let resetPasswordURL = server_url+"reset-password"
let accountStatusURL = server_url + "account-status"
let socialLoginURL = server_url+"social-login"
let isLoginFirstTimeUrl = server_url+"is-login-first-time"
let getUserInfoURL = server_url+"get-user-info"
let updateUserInfoURL = server_url+"update-user-info"
let logoutURL = server_url+"logout"
let deleteAccountURL = server_url+"delete-account"
let payForParkURL = server_url+"pay-for-park"
let createAuction = server_url+"auction-sell"
let getAuctionURL = server_url+"get-auction-list"
let getTimerURL = server_url+"get-timmer"
let endTimerURL = server_url+"end-timmer"
let extendedTimeURL = server_url+"extended-time"
let transactionHistoryURL = server_url+"transaction-history"
let auctionTimerURL = server_url+"get-auction-timmer"
let endAuctionURL = server_url+"end-auction-timmer"
let placeBidURL = server_url+"place-bid"
let GET_AUCTION_DETAIL = server_url + "get-auction-details/"
let AUCTION_VEHICLE_DETAIL = server_url + "auction-vehicle-details/"
let I_M_SPOT_URL  = server_url + "im-spot"
let transferPaymentURL = server_url + "transfer-payments"
let switchConfirmedURL = server_url + "switch-confirmed"
let switchBuyerConfirmedURL = server_url + "buyer-switch-confirmed"
let stripAccountCreateURL = server_url + "create-account"
let changePasswordURL = server_url+"change/password"
let withdrawAmountURL = server_url+"withdraw/amount"

let registerFotPusgURL = server_url+"register-for-push"
let commissionUrl = server_url+"commission"
let GET_BUYER_TIMMER = server_url+"get-buyer-timmer"
let DELETE_PLACE_BID = server_url+"delete-place-bid"
let appVersion = server_url+"appversion"
let CANCEL_SPOT =  server_url+"cancel-auction"
let GOOGLE_CLIENT_ID = "85272504010-ddo7f63j99deaa587ull9rskhcslr2e7.apps.googleusercontent.com"

//MARK:- Social Key
let EMAIL = "email"
let USER_IDD = "userid"
let USERNAME = "username"
let SOCIAL_provider = "socail_provider"
let USER_DATA = "USER_DATA"
let FACEBOOK = "facebook"
let GOOGLE = "google"
let APPLE = "apple"

let appDelegate = UIApplication.shared.delegate as! AppDelegate
