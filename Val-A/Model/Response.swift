//
//  Response.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import ObjectMapper

class Response: Mappable{
    var success: String?
    var message: String?
    var loginResponse: LoginResponse?
    var vehicleMakeResponse: [VehicleMakeResponse]?
    var vehicleModelResponse: [VehicleModelResponse]?
    var vehicleYearResponse: [VehicleYearResponse]?
    var vehicleColorResponse: [VehicleColorResponse]?
    var parkingTimeDurationResponse: [ParkingTimeDurationResponse]?
    var authentication: String?
    var is_login_first_time: Bool?
    var userInfoResponse: UserInfoResponse?
    var payForParkResponse: PayForParkResponse?
    var zoneListResponse: [ZoneListResponse]?
    var auctionResponse:[AuctionResponse]?
    var transactionHistoryResponse: [TransactionHistoryResponse]?
    var getAuctionResponse: AuctionResponse?
    var getBuyerResponse:GetBuyerResponse?
    var auctionVehicleDetailsResponse:AuctionVehicleDetailsResponse?
    var stripAccountCreateResponse:StripAccountCreateResponse?
    var stripStatusResponse:StripStatusResponse?
    var appVersionResponse:AppVersionResponse?
    var commissionResponse:CommissionResponse?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        loginResponse <- map["data"]
        vehicleMakeResponse <- map["data"]
        vehicleModelResponse <- map["data"]
        vehicleYearResponse <- map["data"]
        vehicleColorResponse <- map["data"]
        parkingTimeDurationResponse <- map["data"]
        authentication <- map["authentication"]
        is_login_first_time <- map["is_login_first_time"]
        userInfoResponse <- map["data"]
        payForParkResponse <- map["data"]
        zoneListResponse <- map["data"]
        transactionHistoryResponse <- map["data"]
        auctionResponse <- map["data"]
        getAuctionResponse <- map["data"]
        getBuyerResponse <- map["data"]
        auctionVehicleDetailsResponse <- map["data"]
        stripAccountCreateResponse <- map["data"]
        stripStatusResponse <- map["data"]
        appVersionResponse <- map["data"]
        commissionResponse <- map["data"]
    }
}
