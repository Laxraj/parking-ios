//
//  RequestModel.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import ObjectMapper

class LoginRequest: Mappable{
    var phone: String?
    var password: String?
    var country_code:String?
    init(phone: String?,password: String?,country_code:String?) {
        self.phone = phone
        self.password = password
        self.country_code = country_code
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        phone <- map["phone"]
        password <- map["password"]
        country_code <- map["country_code"]
    }
}
class SignUpRequest: Mappable {
    var first_name: String?
    var last_name: String?
    var phone: String?
    var password: String?
    var vehicle_make: String?
    var vehicle_model: String?
    var vehicle_year: String?
    var vehicle_color: String?
    var license_plate_no: String?
    var provider_id:String?
    var provider_type:String?
    var country_code:String?
    
    
    init(first_name: String?,last_name: String?,phone: String?,password: String?,vehicle_make: String?,vehicle_model: String?,vehicle_year: String?,vehicle_color: String?,license_plate_no: String?,provider_id:String?,provider_type:String?,country_code:String?) {
        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        self.password = password
        self.vehicle_make = vehicle_make
        self.vehicle_model = vehicle_model
        self.vehicle_year = vehicle_year
        self.vehicle_color = vehicle_color
        self.license_plate_no = license_plate_no
        self.provider_id = provider_id
        self.provider_type = provider_type
        self.country_code = country_code
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone <- map["phone"]
        password <- map["password"]
        vehicle_make <- map["vehicle_make"]
        vehicle_model <- map["vehicle_model"]
        vehicle_year <- map["vehicle_year"]
        vehicle_color <- map["vehicle_color"]
        license_plate_no <- map["license_plate_no"]
        provider_id <- map["provider_id"]
        provider_type <- map["provider_type"]
        country_code <- map["country_code"]
        
    }
}

class ForgotPasswordRequest: Mappable{
    var phone: String?
    var password: String?
    var password_confirmation: String?
    var country_code:String?
    init(phone: String?,password: String?,password_confirmation: String?,country_code:String?) {
        self.phone = phone
        self.password = password
        self.country_code = country_code
        self.password_confirmation = password_confirmation
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        phone <- map["phone"]
        password <- map["password"]
        country_code <- map["country_code"]
        password_confirmation <- map["password_confirmation"]
    }
}
class SocialLoginRequest: Mappable{
    var provider_id: String?
    var provider_type: String?
    var token: String?
    
    init(provider_id: String?,provider_type: String?,token: String?) {
        self.provider_id = provider_id
        self.provider_type = provider_type
        self.token = token
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        provider_id <- map["provider_id"]
        provider_type <- map["provider_type"]
        token <- map["token"]
    }
}
class UpdateUserInfoRequest: Mappable{
    var first_name: String?
    var last_name: String?
    var phone: String?
    var password: String?
    var vehicle_make: String?
    var vehicle_model: String?
    var vehicle_year: String?
    var vehicle_color: String?
    var license_plate_no: String?
    var card_number: String?
    var expiration_date: String?
    var cvv: String?
    var country_code: String?
    
    init(first_name: String?,last_name: String?,phone: String?,password: String?,vehicle_make: String?,vehicle_model: String?,vehicle_year: String?,vehicle_color: String?,license_plate_no: String?,card_number: String?,expiration_date: String?,cvv: String?,country_code: String?) {
        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        self.password = password
        self.vehicle_make = vehicle_make
        self.vehicle_model = vehicle_model
        self.vehicle_year = vehicle_year
        self.vehicle_color = vehicle_color
        self.license_plate_no = license_plate_no
        self.card_number = card_number
        self.expiration_date = expiration_date
        self.cvv = cvv
        self.country_code = country_code
    }
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone <- map["phone"]
        password <- map["password"]
        vehicle_make <- map["vehicle_make"]
        vehicle_model <- map["vehicle_model"]
        vehicle_year <- map["vehicle_year"]
        vehicle_color <- map["vehicle_color"]
        license_plate_no <- map["license_plate_no"]
        card_number <- map["card_number"]
        expiration_date <- map["expiration_date"]
        cvv <- map["cvv"]
        country_code <- map["country_code"]
    }
}
class PayForParkRequest: Mappable{
    var duration_id: String?
    var zone_id: String?
    var transaction_type: String?
    var payment_type: String?
    var price: String?
    var payment_status: String?
    var transaction_id: String?
    
    init(duration_id: String?,zone_id: String?,transaction_type: String?,payment_type: String?,price: String?,payment_status: String?,transaction_id: String?) {
        self.duration_id = duration_id
        self.zone_id = zone_id
        self.transaction_type = transaction_type
        self.payment_type = payment_type
        self.price = price
        self.payment_status = payment_status
        self.transaction_id = transaction_id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        duration_id <- map["duration_id"]
        zone_id <- map["zone_id"]
        transaction_type <- map["transaction_type"]
        payment_type <- map["payment_type"]
        price <- map["price"]
        payment_status <- map["payment_status"]
        transaction_id <- map["transaction_id"]
    }
}

class AuctionSellRequest: Mappable{
    var description: String?
    var time_of_exchange: String?
    var startbid: String?
    var free_paid: String?
    var fee: String?
    var zone_id: String?
    var longitude: String?
    var latitude: String?
    init(description: String?,time_of_exchange: String?,startbid: String?,free_paid: String?,fee: String?,zone_id: String?,longitude: String?,latitude: String?) {
        self.description = description
        self.time_of_exchange = time_of_exchange
        self.startbid = startbid
        self.free_paid = free_paid
        self.fee = fee
        self.zone_id = zone_id
        self.longitude = longitude
        self.latitude = latitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        description <- map["description"]
        time_of_exchange <- map["time_of_exchange"]
        startbid <- map["startbid"]
        free_paid <- map["free_paid"]
        fee <- map["fee"]
        zone_id <- map["zone_id"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
    }
}

class ExtendTimeRequest: Mappable {
    var extended_time: String?
    var zone_id: String?
    var transaction_type: String?
    var payment_type: String?
    var price: String?
    var payment_status: String?
    var transaction_id: String?
    
    init(extended_time: String?,zone_id: String?,transaction_type: String?,payment_type: String?,price: String?,payment_status: String?,transaction_id: String?) {
        self.extended_time = extended_time
        self.zone_id = zone_id
        self.transaction_type = transaction_type
        self.payment_type = payment_type
        self.price = price
        self.payment_status = payment_status
        self.transaction_id = transaction_id
    }
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        extended_time <- map["extended_time"]
        zone_id <- map["zone_id"]
        transaction_type <- map["transaction_type"]
        payment_type <- map["payment_type"]
        price <- map["price"]
        payment_status <- map["payment_status"]
        transaction_id <- map["transaction_id"]
    }
}
class PlaceBidRequest: Mappable {
    var auction_id: String?
    var bid_amount: String?
    
    init(auction_id: String?,bid_amount: String?) {
        self.auction_id = auction_id
        self.bid_amount = bid_amount
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        auction_id <- map["auction_id"]
        bid_amount <- map["bid_amount"]
    }
}


class GetAuctionDetailRequest: Mappable {
    var id: String?
    init(id: String?) {
        self.id = id
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        id <- map["id"]
    }
}
class cancelBidRequest: Mappable {
    var auction_id: String?
    init(auction_id: String?) {
        self.auction_id = auction_id
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        auction_id <- map["auction_id"]
    }
}

class appVersionRequest: Mappable {
    var version: String?
    init(version: String?) {
        self.version = version
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        version <- map["version"]
    }
}


class imSpotRequest: Mappable {
    var auction_id: String?
    init(auction_id: String?) {
        self.auction_id = auction_id
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        auction_id <- map["auction_id"]
    }
}

class RegisterForPushRequest: Mappable {
    var device_id: String?
    var device_type: String?
    var token: String?
    init(device_id: String?,device_type: String?,token: String?) {
        self.device_id = device_id
        self.device_type = device_type
        self.token = token
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        device_id <- map["device_id"]
        device_type <- map["device_type"]
        token <- map["token"]
    }
}
class auctionListRequest: Mappable {
    var longitude: String?
    var latitude: String?
    init(longitude: String?,latitude: String?) {
        self.longitude = longitude
        self.latitude = latitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        longitude <- map["longitude"]
        latitude <- map["latitude"]
    }
}
class OnBoardRequest: Mappable{
    var image: UIImage?
    var description: String?
    init(image: UIImage?,description: String?) {
        self.image = image
        self.description = description
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        description <- map["description"]
    }
}
class ChangePasswordRequest: Mappable {
    var current_password: String?
    var password: String?
    var passwordConfirmation: String?
    
    init(current_password: String?,password: String?,passwordConfirmation: String?) {
        self.current_password = current_password
        self.password = password
        self.passwordConfirmation = passwordConfirmation
    }

    required init?(map: Map){
    }

    func mapping(map: Map) {
        current_password <- map["current_password"]
        password <- map["password"]
        passwordConfirmation <- map["password_confirmation"]
    }
}

class WithdrawPaymentRequest: Mappable {
    var amount: String?

    init(amount: String?) {
        self.amount = amount
    }

    required init?(map: Map){
    }

    func mapping(map: Map) {
        amount <- map["amount"]
    }
}
