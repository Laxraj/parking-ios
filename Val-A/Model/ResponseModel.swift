//
//  ResponseModel.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import ObjectMapper

class LoginResponse: Mappable{
    public var user_id: Int?
    public var first_name: String?
    public var last_name: String?
    public var phone: String?
    public var vehicle_make: String?
    public var vehicle_model: String?
    public var vehicle_year: String?
    public var vehicle_color: String?
    public var license_plate_no: String?
    public var country_code: String?
    public var isOTPVerify: Bool?
    public var profile_pic: String?
    public var auth : Auth?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone <- map["phone"]
        vehicle_make <- map["vehicle_make"]
        vehicle_model <- map["vehicle_model"]
        vehicle_year <- map["vehicle_year"]
        vehicle_color <- map["vehicle_color"]
        license_plate_no <- map["license_plate_no"]
        country_code <- map["country_code"]
        isOTPVerify <- map["isOTPVerify"]
        profile_pic <- map["profile_pic"]
        auth <- map["auth"]
    }
}

class Auth: Mappable {
    var tokenType: String?
    var accessToken: String?
    var refreshToken: String?
    var expiresIn: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        tokenType <- map["tokenType"]
        accessToken <- map["accessToken"]
        refreshToken <- map["refreshToken"]
        expiresIn <- map["expiresIn"]
    }
}
class VehicleMakeResponse: Mappable{
    var vehicle_make_id: Int?
    var name: String?
    var models : [VehicleModelResponse]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        vehicle_make_id <- map["vehicle_make_id"]
        name <- map["name"]
        models <- map["models"]
    }
}

class VehicleModelResponse: Mappable{
    var vehicle_model_id: Int?
    var name: String?
    var id: Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        vehicle_model_id <- map["vehicle_model_id"]
        id <- map["id"]
        name <- map["name"]
    }
}


class VehicleYearResponse: Mappable{
    var vehicle_year_id: Int?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        vehicle_year_id <- map["vehicle_year_id"]
        name <- map["name"]
    }
}

class VehicleColorResponse: Mappable{
    var vehicle_color_id: Int?
    var name: String?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        vehicle_color_id <- map["vehicle_color_id"]
        name <- map["name"]
    }
}
class ParkingTimeDurationResponse: Mappable{
    var id: Int?
    var durations: String?
    var price:Double?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        durations <- map["durations"]
        price <- map["price"]
    }
}

class UserInfoResponse: Mappable{
    var user_id: Int?
    var profile_pic: String?
    var first_name: String?
    var last_name: String?
    var phone: String?
    var vehicle_make: Int?
    var vehicle_make_name: String?
    var vehicle_model: Int?
    var vehicle_model_name: String?
    var vehicle_year: Int?
    var vehicle_year_name: String?
    var vehicle_color: Int?
    var vehicle_color_name: String?
    var license_plate_no: String?
    var card_number: String?
    var expiration_date: String?
    var cvv: String?
    var card_brand: String?
    var country_code:String?
    var is_social_account:Bool?
    var is_login_first_time: Bool?
    var stripe_status:Int?
    var balance:Double?
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        profile_pic <- map["profile_pic"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        phone <- map["phone"]
        vehicle_make <- map["vehicle_make"]
        vehicle_make_name <- map["vehicle_make_name"]
        vehicle_model <- map["vehicle_model"]
        vehicle_model_name <- map["vehicle_model_name"]
        vehicle_year <- map["vehicle_year"]
        vehicle_year_name <- map["vehicle_year_name"]
        vehicle_color <- map["vehicle_color"]
        vehicle_color_name <- map["vehicle_color_name"]
        license_plate_no <- map["license_plate_no"]
        card_number <- map["card_number"]
        expiration_date <- map["expiration_date"]
        cvv <- map["cvv"]
        card_brand <- map["card_brand"]
        country_code <- map["country_code"]
        is_social_account <- map["is_social_account"]
        is_login_first_time <- map["is_login_first_time"]
        stripe_status <- map["stripe_status"]
        balance <- map["balance"]
    }
}
class PayForParkResponse: Mappable{
    var zone_id: String?
    var zone_name: String?
    var expiry_time: String?
    var is_timmer: Int?
    var zone_address: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        zone_id <- map["zone_id"]
        zone_name <- map["zone_name"]
        expiry_time <- map["expiry_time"]
        is_timmer <- map["is_timmer"]
        zone_address <- map["zone_address"]
    }
}
class ZoneListResponse: Mappable{
    var name: String?
    var id: String?
    var zone_address: String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        zone_address <- map["zone_address"]
    }
}

class TransactionHistoryResponse: Mappable {
    var transaction_details_id: Int?
    var date: String?
    var zone: String?
    var transaction_type: String?
    var price: String?
    var payment_type: String?
    var vehicle_model_name: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        transaction_details_id <- map["transaction_details_id"]
        date <- map["date"]
        zone <- map["zone"]
        transaction_type <- map["transaction_type"]
        price <- map["price"]
        payment_type <- map["payment_type"]
        vehicle_model_name <- map["vehicle_model_name"]
    }
}
class AuctionVehicleDetailsResponse: Mappable {
    var user_id:Int?
    var profile_pic:String?
    var first_name:String?
    var last_name:String?
    var country_code:String?
    var phone:String?
    var vehicle_make:Int?
    var vehicle_make_name:String?
    var vehicle_model:Int?
    var vehicle_model_name:String?
    var vehicle_year:Int?
    var vehicle_year_name:String?
    var vehicle_color:Int?
    var vehicle_color_name:String?
    var license_plate_no:String?
    var card_number:String?
    var expiration_date:String?
    var cvv:String?
    var card_brand:String?
    var longitude:String?
    var latitude:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        profile_pic <- map["profile_pic"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        country_code <- map["country_code"]
        phone <- map["phone"]
        vehicle_make <- map["vehicle_make"]
        vehicle_make_name <- map["vehicle_make_name"]
        vehicle_model <- map["vehicle_model"]
        vehicle_model_name <- map["vehicle_model_name"]
        vehicle_color <- map["vehicle_color"]
        vehicle_color_name <- map["vehicle_color_name"]
        vehicle_year_name <- map["vehicle_year_name"]
        license_plate_no <- map["license_plate_no"]
        card_number <- map["card_number"]
        expiration_date <- map["expiration_date"]
        cvv <- map["cvv"]
        card_brand <- map["card_brand"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
    }
}


class AuctionResponse: Mappable {
    var auctions_id: Int?
    var description: String?
    var time_of_exchange: String?
    var startbid: String?
    var free_paid:Int?
    var fee:String?
    var zone_id:String?
    var zone_name:String?
    var longitude:String?
    var latitude:String?
    var is_timmer:Int?
    var currentbid: String?
    var highest_bid:String?
    var menu_type:Int?
    var place_bid:Bool?
    var exchange_sports:Bool?
    var payment:Int?
    var availability:String?
    var zone_address:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        auctions_id <- map["auctions_id"]
        description <- map["description"]
        time_of_exchange <- map["time_of_exchange"]
        startbid <- map["startbid"]
        free_paid <- map["free_paid"]
        fee <- map["fee"]
        zone_id <- map["zone_id"]
        zone_name <- map["zone_name"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        is_timmer <- map["is_timmer"]
        currentbid <- map["currentbid"]
        place_bid <- map["place_bid"]
        highest_bid <- map["highest_bid"]
        exchange_sports <- map["exchange_sports"]
        payment <- map["payment"]
        menu_type <- map["menu_type"]
        zone_address <- map["zone_address"]
        availability <- map["availability"]
        
    }
}

class GetBuyerResponse: Mappable {
    var auction_id: Int?
    var is_timmer:Bool?
    var payforpark: Bool?
    var im_in_spot:Int?
    var payment:Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        auction_id <- map["auction_id"]
        is_timmer <- map["is_timmer"]
        payforpark <- map["payforpark"]
        im_in_spot <- map["im_in_spot"]
        payment <- map["payment"]
    }
}


class StripAccountCreateResponse: Mappable {
    var object: String?
    var created:Int?
    var expires_at: Int?
    var url:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        object <- map["object"]
        created <- map["created"]
        expires_at <- map["expires_at"]
        url <- map["url"]
    }
}

class StripStatusResponse: Mappable {
    var stripe_status: Int?
    var balance:Double?
    var amount:Double?
    var pendingBalance:Double?
    var availableBalance:Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        stripe_status <- map["stripe_status"]
        balance <- map["balance"]
        amount <- map["amount"]
        pendingBalance <- map["pendingBalance"]
        availableBalance <- map["availableBalance"]
    }
}

class AppVersionResponse: Mappable {
    var current_version: String?
    var new_version:String?
    var is_update:Bool?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        current_version <- map["current_version"]
        new_version <- map["new_version"]
        is_update <- map["is_update"]
    }
}

class CommissionResponse: Mappable {
    var commission: Double?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        commission <- map["commission"]
    }
}

   
