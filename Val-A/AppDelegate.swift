//
//  AppDelegate.swift
//  Val-A
//
//  Created by iroid on 08/02/21.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKCoreKit
import Firebase
import MapKit
import FirebaseAuth
import FirebaseMessaging
@main
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {
    
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var fcmToken = ""
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        // Override point for customization after application launch.
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        ApplicationDelegate.shared.application(application,didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENT_ID
        
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self

        locationPermistion()
        Utility.setViewControllerRoot()
        return true
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if (GIDSignIn.sharedInstance()?.handle(url))!{
            return true
        }
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func locationPermistion(){
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLatitude = "0";
        currentLongitude = "0";
        currentLatitude = "\(locValue.latitude)"//(String(format:"%.2f",locValue.latitude) as NSString) as String
        currentLongitude = "\(locValue.longitude)"//(String(format:"%.2f",locValue.longitude) as NSString) as String
        print(currentLatitude)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("error::: \(error)")
        locationManager.stopUpdatingLocation()
    }
    
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
         Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        print(userInfo)
        let type = userInfo["type"] as? String
        print(type)
        if  Utility.getUserData() != nil{
            if let type = userInfo["type"] as? String{
                if type == "1"{
                    NotificationCenter.default.post(name: Notification.Name("NEW_BID"), object: nil)
                }else if type == "5"{
                    NotificationCenter.default.post(name: Notification.Name("PAYMENT_DONE"), object: nil)
                }
            }
        }
        
        
        // Change this to your preferred presentation option
        completionHandler([.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        if  Utility.getUserData() != nil{
            if let type = userInfo["type"] as? String{
                guard let rootVC = STORYBOARD.tabbar.instantiateViewController(withIdentifier: "TabbarScreen") as? TabbarScreen else {
                    return
                }
                // rootVC.storIdString = storeId
                rootVC.type = Int(type) ?? 0
                rootVC.isFromNotification = true
                let navigationController = UINavigationController(rootViewController: rootVC)
                navigationController.isNavigationBarHidden = true
                UIApplication.shared.windows.first?.rootViewController = navigationController
                UIApplication.shared.windows.first?.makeKeyAndVisible()
            }
        }
         
        completionHandler()
    }
    
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)

////         Pass device token to auth.
//           let firebaseAuth = Auth.auth()
//
//           //At development time we use .sandbox
//           firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)

           //At time of production it will be set to .prod
        
//        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        
#if DEVELOPMENT
       //Develop
       Messaging.messaging().setAPNSToken(deviceToken as Data, type: .sandbox)
   #else
       //Production
       Messaging.messaging().setAPNSToken(deviceToken as Data, type: .prod)
   #endif
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        Messaging.messaging().apnsToken = deviceToken as Data
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        UserDefaults().set(token, forKey: "DEVICE_TOKEN")
        if Utility.getUserData() != nil{
            registerForPush()
        }
    }
}
extension AppDelegate : MessagingDelegate{
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        self.fcmToken = fcmToken ?? ""
        //        let dataDict:[String: String] = ["token": fcmToken]
        //        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
}
//MARK:- API METHOD
extension AppDelegate {
    func registerForPush(){
        if Utility.isInternetAvailable(){
            var fcmToken = ""
            if let token = Messaging.messaging().fcmToken {
                print("FCM token: \(token)")
                fcmToken = token
            }
            let data = RegisterForPushRequest(device_id: DEVICE_UNIQUE_IDETIFICATION, device_type: "ios", token: fcmToken)
            print(data.toJSON())
            LoginService.shared.registerForPush(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                //                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
        }
    }
}

