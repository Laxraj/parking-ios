//
//  PlaceBidScreen.swift
//  Val-A
//
//  Created by Nikunj on 12/03/21.
//

import UIKit

class PlaceBidScreen: UIViewController {
    
    
    @IBOutlet weak var incrementPriceLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var currentBidLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var zoneLable: UILabel!
    @IBOutlet weak var placeBidButton: UIButton!
    @IBOutlet weak var cancelButton: dateSportButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var backButton: dateSportButton!
    @IBOutlet weak var backButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var cancelButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var pluesButton: UIButton!
    @IBOutlet weak var priceYourBidView: UIView!
    @IBOutlet weak var yourBidView: UIView!
    @IBOutlet weak var yourbidPriceLabel: UILabel!
  
    @IBOutlet weak var bidTitle: UILabel!
        
    @IBOutlet weak var parkingSummaryLocationLabel: UILabel!
    @IBOutlet weak var parkingSummaryDiscriptionLabel: UILabel!
    @IBOutlet weak var parkingSummaryPaidOrFreeLabel: UILabel!
    @IBOutlet weak var parkingSummaryYourBidLabel: UILabel!
    @IBOutlet weak var parkingSummaryAuctionBidLabel: UILabel!
    @IBOutlet weak var parkingSummaryServiceFeeLabel: UILabel!
    @IBOutlet weak var parkingSummaryTotalAmountLabel: UILabel!
    @IBOutlet weak var parkingSummaryView: dateSportView!
    
    @IBOutlet weak var parkingLocationTopConstant: NSLayoutConstraint!
    @IBOutlet weak var parkingLocationLabelHight: NSLayoutConstraint!
    var auctionObj: AuctionResponse?
    var releaseDate: Date?
    var countdownTimer = Timer()
    var auctionId = 0
    var onClose: (() -> Void)?
    var finalIncrementPrice = 0.0
    var clientCommistion = 0.0
    var isActionOnGoin = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayBackView()
        self.setData()
        self.actionDetail()
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.countdownTimer.invalidate()
    }
    

    
    func setData(){
            if let data = self.auctionObj{
                 if data.zone_name == ""{
                    parkingLocationTopConstant.constant = 0
                    self.zoneLable.text = ""
                    parkingLocationLabelHight.constant = 0
                }else{
                    self.zoneLable.text = data.zone_name
                }
                
                self.yourBidView.isHidden = true
                self.descriptionLabel.text = data.description
                let currantBid =  data.highest_bid == "" ? "\(data.startbid ?? "")" : "\(data.highest_bid ?? "")"
                self.currentBidLabel.text = "$\(String(format:"%.02f", Double(currantBid) ?? 0.0))"
                self.paidLabel.text = data.free_paid == 2  ? "Paid: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour" : "Free: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour"
                let incrementPrice = (data.highest_bid == "0" || data.highest_bid == "") ? "\(data.startbid ?? "0")" : "\(data.highest_bid ?? "0")"
                finalIncrementPrice = (Double(incrementPrice) ?? 0.0) + 0.25
                self.incrementPriceLabel.text = String(format:"%.02f", finalIncrementPrice)
                
                
                if data.zone_name == ""{
                    self.parkingSummaryLocationLabel.text = ""
//                    parkingLocationTopConstant.constant = 0
                }else{
                    self.parkingSummaryLocationLabel.text = "Parking Location: \(data.zone_name ?? "")"
                }
                
                self.parkingSummaryDiscriptionLabel.text = "Description: \(data.description ?? "")"
                self.parkingSummaryPaidOrFreeLabel.text = "Free or Paid: \(data.free_paid == 2  ? "Paid: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour" : "Free: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour")"

                self.parkingSummaryYourBidLabel.text = "Your Bid: $\(String(format:"%.02f", Double(data.currentbid ?? "0") ?? 0.0))"
                
                self.startTimer()
            }
        }
    
    func refreshTimer(){
        self.countdownTimer.invalidate()
        self.setData()
    }
    
    @IBAction func onBack(_ sender: Any) {
        countdownTimer.invalidate()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        self.displayCancel()
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.displayBackView()
    }
    
    @IBAction func onPlaceBid(_ sender: Any) {
        if !isActionOnGoin{
            placeBidActionAPI()
        }else{
            Utility.showAlert(vc: self, message: "You are receiving this message because you are the highest bidder on another auction. You can not place another bid until you are outbidded or cancel your last bid.")
        }
    }
    
    @IBAction func onDismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    func displayBackView(){
        self.parkingSummaryView.isHidden = true
        self.bidTitle.text = "Place a bid"
        self.minusButton.isHidden = false
        self.pluesButton.isHidden = false
        self.priceYourBidView.isHidden = false
        self.yourBidView.isHidden = true
        self.cancelButtonHeight.constant = 0
        self.backButtonHeight.constant = 40
        self.cancelButton.isHidden = true
        self.placeBidButton.isHidden = true
        self.backButton.isHidden = false
        self.confirmButton.isHidden = false
    }
    
    func displayCancel(){
        self.parkingSummaryView.isHidden = false
        self.bidTitle.text = "Confirm bid"
        self.yourBidView.isHidden = false
        self.minusButton.isHidden = true
        self.pluesButton.isHidden = true
        self.priceYourBidView.isHidden = true
        self.cancelButtonHeight.constant = 40
        self.backButtonHeight.constant = 0
        self.cancelButton.isHidden = false
        self.placeBidButton.isHidden = false
        self.backButton.isHidden = true
        self.confirmButton.isHidden = true
        self.parkingSummaryAuctionBidLabel.text = "Auction Bid $\(incrementPriceLabel.text ?? "")"
        self.parkingSummaryYourBidLabel.text = "Auction Bid $\(incrementPriceLabel.text ?? "")"
        self.yourbidPriceLabel.text = "$\(incrementPriceLabel.text ?? "")"
        self.totalAmount(auctionBid: (Double(incrementPriceLabel.text ?? "0") ?? 0.0))
    }
    
    func totalAmount(auctionBid:Double){
//        let totalAppPer = auctionBid*clientCommistion/100
        
                let totalStripPrice = auctionBid*50/100
        
                let totalFees = totalStripPrice + 0.60

        
//        let totalStripPrice = auctionBid*2.9/100
//        let totalFees = totalAppPer + totalStripPrice + 0.30
        self.parkingSummaryServiceFeeLabel.text = "Sevice fee: $\(String(format:"%.02f",totalFees))"
        let totalAmount = totalFees + auctionBid
        self.parkingSummaryTotalAmountLabel.text = "Sub Total: $\(String(format:"%.02f",totalAmount))"
    }
    
    func startTimer() {
        let releaseDateString = self.auctionObj?.time_of_exchange ?? ""
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.releaseDate = releaseDateFormatter.date(from: releaseDateString)! as Date
        self.countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
        let currentDate = Date()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: self.releaseDate! as Date)
        
        let countdown = "Days \(diffDateComponents.day ?? 0), Hours \(diffDateComponents.hour ?? 0), Minutes \(diffDateComponents.minute ?? 0), Seconds \(diffDateComponents.second ?? 0)"
        
        self.timerLabel.text = String(format: "%02d:%02d:%02d", diffDateComponents.hour ?? 0, diffDateComponents.minute ?? 0, diffDateComponents.second ?? 0)
        
        print(countdown)
        let second =   Double(diffDateComponents.second ?? 0)
        if second.sign == .minus{
            self.countdownTimer.invalidate()
            self.dismiss(animated: false, completion: { [weak self] in
                self?.onClose?()
            })
        }
        
//        if diffDateComponents.hour == 0 && diffDateComponents.minute == 0 && diffDateComponents.second == 0{
//            self.countdownTimer.invalidate()
//
//            //            self.navigationController?.popViewController(animated: true)
//        }
    }
    
    @IBAction func onMinus(_ sender: Any) {
      
        var price = Double(self.incrementPriceLabel.text ?? "0.0") ?? 0.0
        
        let origionalPrice = Double(self.finalIncrementPrice)
        price -= 0.25
        guard price >= origionalPrice else {
            return
        }
        
        self.incrementPriceLabel.text = String(format:"%.02f", price)
   
    }
    @IBAction func onPlus(_ sender: Any) {
        var price = Double(self.incrementPriceLabel.text ?? "0.0") ?? 0.0
        price += 0.25
        self.incrementPriceLabel.text = String(format:"%.02f", price)
    }
    //MARK:-  Zone Location API
    func placeBidActionAPI(){
        self.view.endEditing(true)
        let finalPrice = Double(self.incrementPriceLabel.text ?? "0.0") ?? 0.0
//        let paidPrice = Double(self.paidLabel.text ?? "0.0") ?? 0.0
//        let finalPrice = bidPrice + paidPrice
        
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = PlaceBidRequest(auction_id: "\(auctionId)", bid_amount:"\(finalPrice)")
            AuctionServices.shared.placeBid(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                self.dismiss(animated: false, completion: { [weak self] in
                    self?.onClose?()
                })
                
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    //MARK:-  Zone Location API
    func actionDetail(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            let url = GET_AUCTION_DETAIL + "\(auctionId)"
            AuctionServices.shared.actionDetail(URLString: url){ (statusCode, response) in
//                Utility.hideIndicator()
                print(response.toJSON())
                
                if let data = response.getAuctionResponse{
                    self.auctionObj = data
//                    self.setData()
                    self.refreshTimer()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
   
}
