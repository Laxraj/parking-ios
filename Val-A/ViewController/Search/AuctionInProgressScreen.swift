//
//  AuctionInProgressScreen.swift
//  Val-A
//
//  Created by iroid on 21/03/21.
//

import UIKit

class AuctionInProgressScreen: UIViewController {

    var auctionId = 0
    var onCancelBid: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func cancelAction(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = cancelBidRequest(auction_id: "\(auctionId)")
            AuctionServices.shared.cancelPlaceBid(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                self.dismiss(animated: false, completion: { [weak self] in
                    self?.onCancelBid?()
                })
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        cancelAction()
    }
    
    @IBAction func onStay(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
