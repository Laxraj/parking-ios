//
//  AuctionSummaryScrren.swift
//  Val-A
//
//  Created by iroid on 21/03/21.
//

import UIKit

class AuctionSummaryScrren: UIViewController {

    @IBOutlet weak var parkingLocationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var freeOrPaidLabel: UILabel!
    @IBOutlet weak var yourBidLabel: UILabel!
    @IBOutlet weak var timeRemaingLabel: UILabel!
    @IBOutlet weak var highestBidLabel: UILabel!
    @IBOutlet weak var yourNewBidLabel: UILabel!
    @IBOutlet weak var yourBidView: UIView!
    @IBOutlet weak var pluesButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var priceYourBidView: UIView!
    @IBOutlet weak var confirmCancelButton: UIButton!
    @IBOutlet weak var placeBidButton: UIButton!
    @IBOutlet weak var confirmBidLabel: UILabel!
    @IBOutlet weak var parkingLocationTopConstant: NSLayoutConstraint!
    
    
    @IBOutlet weak var parkingSummaryLocationLabel: UILabel!
    @IBOutlet weak var parkingSummaryDiscriptionLabel: UILabel!
    @IBOutlet weak var parkingSummaryPaidOrFreeLabel: UILabel!
    @IBOutlet weak var parkingSummaryYourBidLabel: UILabel!
    @IBOutlet weak var parkingSummaryAuctionBidLabel: UILabel!
    @IBOutlet weak var parkingSummaryServiceFeeLabel: UILabel!
    @IBOutlet weak var parkingSummaryTotalAmountLabel: UILabel!
    @IBOutlet weak var parkingSummaryView: dateSportView!
    
    var auctionObj: AuctionResponse?
    var releaseDate: Date?
    var countdownTimer = Timer()
    var auctionId = 0
    var onClose: (() -> Void)?
    var isActionOnGoin = false
    var startNewBid: Double = 0.0
    var clientCommistion = 0.0
    var finalIncrementPrice = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmCancelButton.isHidden = true
        placeBidButton.isHidden = true
        self.yourBidView.isHidden = true
        self.parkingSummaryView.isHidden = true
        actionDetail()
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.countdownTimer.invalidate()
        dismiss(animated: false, completion: nil)
    }
   
    func setData(){
            if let data = self.auctionObj{
                if data.zone_name == ""{
                    self.parkingLocationLabel.text = ""
                    parkingLocationTopConstant.constant = 0
                }else{
                    self.parkingLocationLabel.text = "Parking Location: \(data.zone_name ?? "")"
                }
                
                self.descriptionLabel.text = "Description: \(data.description ?? "")"
                self.freeOrPaidLabel.text = "Free or Paid: \(data.free_paid == 2  ? "Paid: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour" : "Free: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour")"

                self.yourBidLabel.text = "Your Bid: $\(String(format:"%.02f", Double(data.currentbid ?? "0") ?? 0.0))"
                
                self.yourNewBidLabel.text = data.highest_bid
                self.highestBidLabel.text = "$\(String(format:"%.02f", Double(data.currentbid ?? "0") ?? 0.0))"
                self.startNewBid = Double(data.highest_bid ?? "0.0") ?? 0.0
                self.startNewBid += 0.25
                
                let incrementPrice = (data.highest_bid == "0" || data.highest_bid == "") ? "\(data.startbid ?? "0")" : "\(data.highest_bid ?? "0")"
                finalIncrementPrice = (Double(incrementPrice) ?? 0.0) + 0.25
                self.yourNewBidLabel.text = String(format:"%.02f", finalIncrementPrice)
                //            self.yourNewBidLabel.text = "\(self.startNewBid)"
                
                
                if data.zone_name == ""{
                    self.parkingSummaryLocationLabel.text = ""
//                    parkingLocationTopConstant.constant = 0
                }else{
                    self.parkingSummaryLocationLabel.text = "Parking Location: \(data.zone_name ?? "")"
                }
                
                self.parkingSummaryDiscriptionLabel.text = "Description: \(data.description ?? "")"
                self.parkingSummaryPaidOrFreeLabel.text = "Free or Paid: \(data.free_paid == 2  ? "Paid: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour" : "Free: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour")"

                self.parkingSummaryYourBidLabel.text = "Your Bid: $\(String(format:"%.02f", Double(data.currentbid ?? "0") ?? 0.0))"
                
                self.countdownTimer.invalidate()
                self.startTimer()
                
            }
        }
    func totalAmount(auctionBid:Double){
//        let totalAppPer = auctionBid*25/100
        let totalStripPrice = auctionBid*50/100
        let totalFees = totalStripPrice + 0.60

//        let totalStripPrice = auctionBid*2.9/100
//        let totalFees = totalAppPer + totalStripPrice + 0.30
        self.parkingSummaryServiceFeeLabel.text = "Sevice fee: $\(String(format:"%.02f",totalFees))"
        let totalAmount = totalFees + auctionBid
        self.parkingSummaryTotalAmountLabel.text = "Sub Total: $\(String(format:"%.02f",totalAmount))"
    }
    @IBAction func onCancel(_ sender: UIButton) {
        let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "AuctionInProgressScreen") as! AuctionInProgressScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.auctionId = auctionId
        vc.onCancelBid = { [weak self] in
            self?.dismiss(animated: false, completion: { [weak self] in
                    self?.countdownTimer.invalidate()
                    self?.onClose?()
            })
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onConfirm(_ sender: UIButton) {
        self.yourBidView.isHidden = false
        self.confirmCancelButton.isHidden = false
        self.placeBidButton.isHidden = false
        self.parkingSummaryView.isHidden = false
        self.priceYourBidView.isHidden = true
        self.pluesButton.isHidden = true
        self.minusButton.isHidden = true
        self.confirmBidLabel.text = yourNewBidLabel.text
        self.parkingSummaryAuctionBidLabel.text = "Auction Bid $\(yourNewBidLabel.text ?? "")"
        self.parkingSummaryYourBidLabel.text = "Your Bid:  $\(yourNewBidLabel.text ?? "")"
        self.totalAmount(auctionBid: (Double(yourNewBidLabel.text ?? "0") ?? 0.0))
       
    }
    
    @IBAction func onConfirmCancel(_ sender: UIButton) {
        if isActionOnGoin{
            self.dismiss(animated: true, completion: nil)
        }else{
            showConfirmDetail()
        }
        
    }
    
    
    @IBAction func onDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func showConfirmDetail(){
        self.yourBidView.isHidden = true
        self.confirmCancelButton.isHidden = true
        self.placeBidButton.isHidden = true
        self.parkingSummaryView.isHidden = true
        self.priceYourBidView.isHidden = false
        self.pluesButton.isHidden = false
        self.minusButton.isHidden = false
    }
    @IBAction func onPlaceBid(_ sender: UIButton) {
        if !isActionOnGoin{
        placeBidActionAPI()
        }else{
            Utility.showAlert(vc: self, message: "You are receiving this message because you are the highest bidder on another auction. You can not place another bid until you are outbidded or cancel your last bid.")
        }
    }
    
    func startTimer() {
        let releaseDateString = self.auctionObj?.time_of_exchange ?? ""
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.releaseDate = releaseDateFormatter.date(from: releaseDateString)! as Date
        self.countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
        let currentDate = Date()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: self.releaseDate! as Date)
        
        let countdown = "Days \(diffDateComponents.day ?? 0), Hours \(diffDateComponents.hour ?? 0), Minutes \(diffDateComponents.minute ?? 0), Seconds \(diffDateComponents.second ?? 0)"
        
        self.timeRemaingLabel.text = String(format: "%02d:%02d:%02d", diffDateComponents.hour ?? 0, diffDateComponents.minute ?? 0, diffDateComponents.second ?? 0)
        
        print(countdown)
        let second =   Double(diffDateComponents.second ?? 0)
        if second.sign == .minus{
            self.countdownTimer.invalidate()
            self.dismiss(animated: false, completion: { [weak self] in
                self?.countdownTimer.invalidate()
                self?.onClose?()
            })
        }
//        if second.sign == .minus{
//            self.dismiss(animated: false, completion: { [weak self] in
//                self?.countdownTimer.invalidate()
//                self?.onClose?()
//            })
//        }
    }
    
    @IBAction func onMinus(_ sender: Any) {
//        var price = self.startNewBid
//        let origionalPrice = (Double(self.auctionObj?.highest_bid ?? "0.0") ?? 0.0) + 0.25
//        price -= 0.25
//        guard price >= origionalPrice else {
//            return
//        }
//        self.yourNewBidLabel.text = String(format:"%.02f", price)
        
        
        var price = Double(self.yourNewBidLabel.text ?? "0.0") ?? 0.0
        
        let origionalPrice = Double(self.finalIncrementPrice)
        price -= 0.25
        guard price >= origionalPrice else {
            return
        }
        
        self.yourNewBidLabel.text = String(format:"%.02f", price)
    }
    
    @IBAction func onPlus(_ sender: Any) {
//        var price = self.startNewBid
//        price += 0.25
//        self.yourNewBidLabel.text = String(format:"%.02f", price)
        
        var price = Double(self.yourNewBidLabel.text ?? "0.0") ?? 0.0
        price += 0.25
        self.yourNewBidLabel.text = String(format:"%.02f", price)
    }
    //MARK:-  Zone Location API
    func placeBidActionAPI(){
        self.view.endEditing(true)
        let finalPrice = Double(self.yourNewBidLabel.text ?? "0.0") ?? 0.0
        
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = PlaceBidRequest(auction_id: "\(auctionId)", bid_amount:"\(finalPrice)")
            AuctionServices.shared.placeBid(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                self.showConfirmDetail()
                self.actionDetail()
               // self.yourBidLabel.text = "Your Bid: $\(data.currentbid ?? "+")"
                
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:-  Action detail API
    func actionDetail(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            let url = GET_AUCTION_DETAIL + "\(auctionId)"
            AuctionServices.shared.actionDetail(URLString: url){ (statusCode, response) in
//                Utility.hideIndicator()
                print(response.toJSON())
                
                if let data = response.getAuctionResponse{
                    self.auctionObj = data
                    self.setData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    deinit {
    print("remove from memory")
    }
}

