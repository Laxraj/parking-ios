//
//  IMAtTheSpotScreen.swift
//  Val-A
//
//  Created by iroid on 24/03/21.
//

import UIKit
import MapKit
class IMAtTheSpotScreen: UIViewController {

   
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var vahicleImageView: dateSportImageView!
    
    
    var vahicaleDetail: AuctionVehicleDetailsResponse?
    var auctionId = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        actionVehicleDetail()
    }
    
    func setData(){
        

        if let data = self.vahicaleDetail{
//            Utility.setImage(data.profile_pic, imageView: vahicleImageView)
//            self.yearLabel.text = "Year:\(data.vehicle_year_name ?? "")"
//            self.colorLabel.text = "Color:\(data.vehicle_color_name ?? "")"
//            self.makeLabel.text = "Color:\(data.vehicle_make_name ?? "")"
//            self.plateLabel.text = "Color:\(data.license_plate_no ?? "")"
            
            Utility.setImage(data.profile_pic, imageView: vahicleImageView)
            self.yearLabel.text = "Year:\(data.vehicle_year_name ?? "")"
            self.colorLabel.text = "Color:\(data.vehicle_color_name ?? "")"
            self.makeLabel.text = "Make:\(data.vehicle_make_name ?? "")"
            self.modelLabel.text = "Model:\(data.vehicle_model_name ?? "")"
            self.plateLabel.text = "Plate #:\(data.license_plate_no ?? "")"
        }
    }
    
    
    @IBAction func onCancel(_ sender: UIButton) {
        let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "CancelSpotScreen") as! CancelSpotScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.auctionId = auctionId
        vc.onCancelSpot = { [weak self] in
            Utility.setTabRoot()
        }
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onOpenMap(_ sender: UIButton) {

//        let currantLocation = "\(currentLatitude)" + "," + "\(currentLongitude)"
//        let actionLocation = "\(vahicaleDetail?.latitude ?? "")" + "," + "\(vahicaleDetail?.longitude ?? "")"
//        let directionsURL = "http://maps.apple.com/?saddr=\(currantLocation)&daddr=\(actionLocation)"
//        guard let url = URL(string: directionsURL) else {
//            return
//        }
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(url)
//        }
//
        
        let latituide = ((self.vahicaleDetail?.latitude ?? "0.0") as NSString).doubleValue
        let longitude = ((self.vahicaleDetail?.longitude ?? "0.0") as NSString).doubleValue
        
        let coordinate = CLLocationCoordinate2DMake(latituide,longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        mapItem.name = "Parking Spot"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    @IBAction func onArrvidedAtParkingSpot(_ sender: UIButton) {
        arrivedAtParkingSpot()
    }
    
    
    //MARK:-  Action Vehicl Detail API
    func actionVehicleDetail(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            let url = AUCTION_VEHICLE_DETAIL + "\(auctionId)"
            AuctionServices.shared.actionDetail(URLString: url){ (statusCode, response) in
                if let data = response.auctionVehicleDetailsResponse{
                    self.vahicaleDetail = data
                    self.setData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    //MARK:-  arrived Parking Spot API
    func arrivedAtParkingSpot(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = imSpotRequest(auction_id: "\(auctionId)")
            AuctionServices.shared.imSpot(parameters:data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "AuctionPaymentScreen") as! AuctionPaymentScreen
                vc.auctionId = self.auctionId
                self.navigationController?.pushViewController(vc, animated: true)
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }

            
           
        }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
 
    
    
}
