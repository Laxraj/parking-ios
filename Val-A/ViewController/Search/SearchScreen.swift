//
//  SearchScreen.swift
//  Val-A
//
//  Created by iroid on 20/02/21.
//

import UIKit
import MapKit
class SearchScreen: UIViewController {

    @IBOutlet weak var auctionCollectionView: UICollectionView!
    @IBOutlet weak var whereToTextField: UITextField!
    @IBOutlet weak var auctionView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    var auctionList:[AuctionResponse] = []
    var selectedIndexPAth = -1
    var latitute = ""
    var logitute = ""
    var isActionOnGoin = false
    var currantAuction = -1
    var clientCommistion = 0.0
    var getBuyerRespose:GetBuyerResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDetail()
    }
    

    func initialDetail(){
        auctionView.isHidden = true
        mapView.delegate = self
        let nib = UINib(nibName: "AuctionCollectionViewCell", bundle: nil)
        auctionCollectionView.register(nib, forCellWithReuseIdentifier: "AuctionCollectionViewCell")
        latitute = currentLatitude
        logitute = currentLongitude
        self.getClientCommissionAPI()
       // getAuctionListAPI()
        setUpCurrantLocation()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedIndexPAth = -1
        auctionCollectionView.reloadData()
        getAuctionListAPI()
        getParkingTimeDurationAPI()
    }
    
    // MARK: center Map On UserLocation
    func setUpCurrantLocation(){
        let latDelta:CLLocationDegrees = 0.10
        let longDelta:CLLocationDegrees = 0.10
        let theSpan:MKCoordinateSpan =  MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(latitute) ?? 0, Double(logitute) ?? 0)
        let region:MKCoordinateRegion = MKCoordinateRegion(center: pointLocation, span: theSpan)
        self.mapView.showsUserLocation = true
        self.mapView.showsCompass = true
        self.mapView.setRegion(region, animated: true)
//        self.mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLatitude = "0";
        currentLongitude = "0";
        currentLatitude = "\(locValue.latitude)"//(String(format:"%.2f",locValue.latitude) as NSString) as String
        currentLongitude = "\(locValue.longitude)"//(String(format:"%.2f",locValue.longitude) as NSString) as String
        print(currentLatitude)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("error::: \(error)")
//        locationManager.stopUpdatingLocation()
    }
    @IBAction func onCancel(_ sender: UIButton) {
        removeAllPin()
        if auctionList.count > 0{
            for i in 0...auctionList.count-1{
                let model = auctionList[i]
                
                let latitute = Double(model.latitude ?? "0.0")
                let longitude = Double(model.longitude ?? "0.0")
                let marer = customPin(pinTitle: model.zone_name ?? "", Location: CLLocationCoordinate2D(latitude: latitute ?? 0.0, longitude:  longitude ?? 0.0), storeImage:  model.menu_type == 1 ? "map_pin":"blue_small_map_pin", actionId: model.auctions_id ?? 0)
                self.mapView.addAnnotation(marer)
            }
        }
        whereToTextField.text = ""
        latitute = currentLatitude
        logitute = currentLongitude
        getAuctionListAPI()
        auctionView.isHidden = true
    }
    
    @IBAction func onSearchPlace(_ sender: UIButton) {
        let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "SearchLocationScreen") as! SearchLocationScreen
        vc.placeDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onSearch(_ sender: UIButton) {
        setUpCurrantLocation()
        getAuctionListAPI()
        auctionView.isHidden = false
    }
    @IBAction func onNavigateCurrantLocation(_ sender: UIButton) {
        self.setUpCurrantLocation()
    }
    
    
    class func setPayForParkTabRoot(zoneId:String, zoneAddress:String){
        let storyBoard = UIStoryboard(name: "TabBar", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "TabbarScreen") as! TabbarScreen
        control.isPayForPark = true
        control.zoneID = zoneId
        control.zoneAddress = zoneAddress
        let navVC = UINavigationController(rootViewController: control)
        appDelegate.window?.rootViewController = navVC
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func setSelectedAuctionNavigation(res:GetBuyerResponse){
        
        if res.is_timmer ?? false{
            let auctionScreen = STORYBOARD.search.instantiateViewController(withIdentifier: "AuctionSummaryScrren") as! AuctionSummaryScrren
            auctionScreen.modalPresentationStyle = .overCurrentContext
            auctionScreen.auctionId = res.auction_id ?? 0
            auctionScreen.clientCommistion = clientCommistion
            auctionScreen.isActionOnGoin = currantAuction == res.auction_id ? false:isActionOnGoin
            auctionScreen.onClose = { [weak self] in
                self?.getParkingTimeDurationAPI()
            }
            self.present(auctionScreen, animated: true, completion: nil)
        }else if res.im_in_spot == 1 || res.payment == 1{
            let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "AuctionPaymentScreen") as! AuctionPaymentScreen
            vc.modalPresentationStyle = .overCurrentContext
            vc.auctionId = res.auction_id ?? 0
            vc.getBuyerTimerData = res
            self.navigationController?.pushViewController(vc, animated: true)
        }else if res.payforpark ?? false{
            let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "IMAtTheSpotScreen") as! IMAtTheSpotScreen
            vc.modalPresentationStyle = .overCurrentContext
            vc.auctionId = res.auction_id ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func removeAllPin(){
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
    }
    
    func setPin(auctionList:[AuctionResponse]){
        if auctionList.count > 0{
            for i in 0...auctionList.count - 1{
                let model = auctionList[i]
                let latitute = Double(model.latitude ?? "0.0")
                let longitude = Double(model.longitude ?? "0.0")
                let marer = customPin(pinTitle: model.zone_name ?? "", Location: CLLocationCoordinate2D(latitude: latitute ?? 0.0, longitude:  longitude ?? 0.0), storeImage: model.menu_type == 1 ? "map_pin":"blue_small_map_pin", actionId: model.auctions_id ?? 0)
                self.mapView.addAnnotation(marer)
            }
        }
    }
    
    //MARK:-  Zone Location API
    func getAuctionListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            let data = auctionListRequest(longitude:logitute  , latitude: latitute)
            print(data.toJSON())
            AuctionServices.shared.getAuctionList(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                self.removeAllPin()
                print(response?.toJSON())
                if let res = response{
                    self.setPin(auctionList: res)
                    self.auctionList = res
                    self.auctionCollectionView.reloadData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    //MARK:-  Zone Location API
    func getParkingTimeDurationAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            AuctionServices.shared.getAuctionTimeDurationList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.getBuyerResponse{
                    self.getBuyerRespose = res
                    self.isActionOnGoin = res.is_timmer ?? false
                    self.currantAuction = res.auction_id ?? -1
                    self.setSelectedAuctionNavigation(res: res)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getClientCommissionAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            AuctionServices.shared.clientCommission{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.commissionResponse{
                    self.clientCommistion = res.commission ?? 0
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
}
extension SearchScreen : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return auctionList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AuctionCollectionViewCell", for: indexPath) as! AuctionCollectionViewCell
        cell.selectedIndex = selectedIndexPAth
        cell.setUpData(model: auctionList[indexPath.row], indexPath: indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2.4, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let res = Utility.getUserInfoData(){
            if res.card_number != ""{
                selectedIndexPAth = indexPath.row
                auctionCollectionView.reloadData()
                
                let selectedPin = self.auctionList[indexPath.row]
                
                if selectedPin.menu_type == 1{
                    currantAuction = selectedPin.auctions_id ?? -1
                    if getBuyerRespose?.auction_id == auctionList[indexPath.row].auctions_id{
                        setSelectedAuctionNavigation(res: getBuyerRespose!)
                    }else{
                        let vc = STORYBOARD.search.instantiateViewController(withIdentifier: "PlaceBidScreen") as! PlaceBidScreen
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.auctionObj = selectedPin
                        vc.isActionOnGoin = isActionOnGoin
                        vc.auctionId = selectedPin.auctions_id ?? 0
                        vc.clientCommistion = clientCommistion
                        vc.onClose = { [weak self] in
                            self?.isActionOnGoin = true
                            let auctionSummaryScreen = STORYBOARD.search.instantiateViewController(withIdentifier: "AuctionSummaryScrren") as! AuctionSummaryScrren
                            auctionSummaryScreen.modalPresentationStyle = .overCurrentContext
                            auctionSummaryScreen.auctionId = selectedPin.auctions_id ?? 0
                            vc.clientCommistion = self?.clientCommistion ?? 0
                            auctionSummaryScreen.onClose = { [weak self] in
                                self?.getParkingTimeDurationAPI()
                            }
                            self?.present(auctionSummaryScreen, animated: true, completion: nil)
                        }
                        self.present(vc, animated: true, completion: nil)
                    }
                }else{
                    SearchScreen.setPayForParkTabRoot(zoneId: selectedPin.zone_id ?? "0", zoneAddress: selectedPin.zone_address ?? "0")
                }
                removeAllPin()
                for i in 0...auctionList.count-1{
                    let model = auctionList[i]
                    if selectedPin.auctions_id == model.auctions_id{
                        print(i)
                        let latitute = Double(selectedPin.latitude ?? "0.0")
                        let longitude = Double(selectedPin.longitude ?? "0.0")
                        let marer = customPin(pinTitle: selectedPin.zone_name ?? "", Location: CLLocationCoordinate2D(latitude: latitute ?? 0.0, longitude:  longitude ?? 0.0), storeImage: model.menu_type == 1 ? "pin_big_image":"blue_nap_pin_big_image", actionId: selectedPin.auctions_id ?? 0)
                        self.mapView.addAnnotation(marer)
                        selectedIndexPAth = i
                        auctionCollectionView.reloadData()
                    }else{
                        let latitute = Double(model.latitude ?? "0.0")
                        let longitude = Double(model.longitude ?? "0.0")
                        let marer = customPin(pinTitle: model.zone_name ?? "", Location: CLLocationCoordinate2D(latitude: latitute ?? 0.0, longitude:  longitude ?? 0.0), storeImage: model.menu_type == 1 ? "map_pin":"blue_small_map_pin", actionId: model.auctions_id ?? 0)
                        self.mapView.addAnnotation(marer)
                    }
                }
                
            }else{
                Utility.showAlert(vc: self, message: "Please enter card number in your profile")
            }
        }
        
    }
}


extension SearchScreen:MKMapViewDelegate{
    

    // MARK: Set Pin On MapView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            annotationView?.canShowCallout = true
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            let cpa = annotation as! customPin
            pin.image = UIImage(named: cpa.storeImage ?? "")
            pin.isEnabled = true
            pin.canShowCallout = true
            let storeImageView = UIImageView()
            storeImageView.frame = CGRect(x: -25, y: -25, width: 34, height: 42)
            
            if annotation is customPin{
                let cpa = annotation as? customPin
                storeImageView.image = UIImage(named: "")
                pin.addSubview(storeImageView)
                annotationView = pin
            }
            
        }else {
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            
            let cpa = annotation as! customPin
            pin.image = UIImage(named: cpa.storeImage ?? "")
            pin.isEnabled = true
            pin.canShowCallout = true
            
            let storeImageView = UIImageView()
            storeImageView.frame = CGRect(x: -25, y: -25, width: 34, height: 42)
            storeImageView.layer.cornerRadius = storeImageView.frame.height/2
            storeImageView.layer.masksToBounds = true
            storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            storeImageView.layer.borderWidth = 2
            
            let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
            label.textAlignment = .center
            label.textColor = .white
            label.font = label.font.withSize(14)
           
//            label.text = cpa.tag
            storeImageView.image = UIImage(named: cpa.storeImage ?? "")
//            pin.addSubview(storeImageView)
//            pin.addSubview(label)
            annotationView = pin
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        
        if view.annotation is MKUserLocation {
            return
        }
        let pin = view.annotation as! customPin
        print(pin.actionId)
        auctionView.isHidden = false
        removeAllPin()
        for i in 0...auctionList.count-1{
            let model = auctionList[i]
            
            if pin.actionId == model.auctions_id{
                print(i)
                let marer = customPin(pinTitle: pin.title ?? "", Location: pin.coordinate, storeImage: model.menu_type == 1 ? "pin_big_image":"blue_nap_pin_big_image", actionId: model.auctions_id ?? 0)
                self.mapView.addAnnotation(marer)
                selectedIndexPAth = i
                auctionCollectionView.reloadData()
            }else{
                let latitute = Double(model.latitude ?? "0.0")
                let longitude = Double(model.longitude ?? "0.0")
                let marer = customPin(pinTitle: model.zone_name ?? "", Location: CLLocationCoordinate2D(latitude: latitute ?? 0.0, longitude:  longitude ?? 0.0), storeImage:  model.menu_type == 1 ? "map_pin":"blue_small_map_pin", actionId: model.auctions_id ?? 0)
                self.mapView.addAnnotation(marer)
            }
        }
    }
}
extension SearchScreen: PlaceSelectionDelegate{
   
    func placeData(name: String?, latitude: String, longtitude: String) {
        self.latitute = latitude
        self.logitute = longtitude
        self.whereToTextField.text = name
        
    }
}
