//
//  SearchLocationScreen.swift
//  Rejoi
//
//  Created by iroid on 14/11/19.
//  Copyright © 2019 iroid. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

protocol PlaceSelectionDelegate : class{
    func placeData(name: String?,latitude: String,longtitude: String)
}
class SearchLocationScreen: UIViewController,UITextFieldDelegate  {
   
    @IBOutlet weak var locationTableView: UITableView!
    @IBOutlet weak var searchLocationTextField: UITextField!
    


    weak var placeDelegate: PlaceSelectionDelegate?
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    var searchCoordinatesResults = [MKCoordinateRegion]()

    override func viewDidLoad() {
        super.viewDidLoad()
        locationTableView.tableFooterView = UIView()
        searchLocationTextField.delegate = self
        // For use when the app is open & in the background
       
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
  
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests( withTarget: self, selector: #selector(checkUserNameTimer), object: textField)
        self.perform(#selector(checkUserNameTimer), with: textField, afterDelay: 0.5)
        return true
    }
    
    @objc func checkUserNameTimer(){
         checkUserName()
    }
    
    func checkUserName() {
        searchCompleter.delegate = self        
        searchCompleter.queryFragment = searchLocationTextField.text!
        searchCompleter.accessibilityLanguage = "en-US"
        locationTableView.dataSource = self
    }
}
extension SearchLocationScreen: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }
}

extension SearchLocationScreen: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let completion = searchResults[indexPath.row]
        let searchRequest = MKLocalSearch.Request(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, error) in
            let coordinate = response?.mapItems[0].placemark.coordinate
            print(String(describing: coordinate))
            self.placeDelegate?.placeData(name: response?.mapItems.first?.placemark.name, latitude: response?.mapItems.first?.placemark.coordinate.latitude.description ?? "", longtitude: response?.mapItems.first?.placemark.coordinate.longitude.description ?? "")
            self.dismiss(animated: true, completion: nil)
        }
    }
}
extension SearchLocationScreen: MKLocalSearchCompleterDelegate {

    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        let searchCoordinatesResults = completer.region
        print(searchCoordinatesResults)
        print(searchResults)
        print(completer.region)
        locationTableView.reloadData()
    }

    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // handle error
    }
}

