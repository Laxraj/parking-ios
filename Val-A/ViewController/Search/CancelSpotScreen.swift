//
//  CancelSpotScreen.swift
//  Val-A
//
//  Created by iroid on 28/03/21.
//

import UIKit

class CancelSpotScreen: UIViewController {

    var onCancelSpot: (() -> Void)?
    var auctionId = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func onCancel(_ sender: UIButton) {
        cancelAction()
    }
    
    @IBAction func onStay(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelAction(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = cancelBidRequest(auction_id: "\(auctionId)")
            AuctionServices.shared.cancelSpot(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                self.dismiss(animated: false, completion: { [weak self] in
                    self?.onCancelSpot?()
                })
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
  

}
