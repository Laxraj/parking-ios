//
//  AuctionPaymentScreen.swift
//  Val-A
//
//  Created by iroid on 02/04/21.
//

import UIKit
import MapKit
class AuctionPaymentScreen: UIViewController {

    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var vahicleImageView: dateSportImageView!
    @IBOutlet weak var payWithCardButton: UIButton!
    @IBOutlet weak var swichConfirmedButton: UIButton!
    
    var vahicaleDetail: AuctionVehicleDetailsResponse?
    var getBuyerTimerData:GetBuyerResponse?
    var auctionId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        if getBuyerTimerData != nil{
            if getBuyerTimerData?.payment == 0{
                payWithCardButton.isHidden = false
                swichConfirmedButton.isHidden = true
            }else{
                payWithCardButton.isHidden = true
                swichConfirmedButton.isHidden = false
            }
        }
        
        actionVehicleDetail()
    }
    
    func setData(){
        if let data = self.vahicaleDetail{
//            Utility.setImage(data.profile_pic, imageView: vahicleImageView)
//            self.yearLabel.text = "Year:\(data.vehicle_year_name ?? "")"
//            self.colorLabel.text = "Color:\(data.vehicle_color_name ?? "")"
//            self.makeLabel.text = "Color:\(data.vehicle_make_name ?? "")"
//            self.plateLabel.text = "Color:\(data.license_plate_no ?? "")"
            
            
            Utility.setImage(data.profile_pic, imageView: vahicleImageView)
            self.yearLabel.text = "Year:\(data.vehicle_year_name ?? "")"
            self.colorLabel.text = "Color:\(data.vehicle_color_name ?? "")"
            self.makeLabel.text = "Make:\(data.vehicle_make_name ?? "")"
            self.modelLabel.text = "Model:\(data.vehicle_model_name ?? "")"
            self.plateLabel.text = "Plate #:\(data.license_plate_no ?? "")"
        }
    }
        
    @IBAction func onPayWithCard(_ sender: UIButton) {
        actionTransferPayment()
    }
    @IBAction func onOpenMap(_ sender: UIButton) {

//        let currantLocation = "\(currentLatitude)" + "," + "\(currentLongitude)"
//        let actionLocation = "\(vahicaleDetail?.latitude ?? "")" + "," + "\(vahicaleDetail?.longitude ?? "")"
//        let directionsURL = "http://maps.apple.com/?saddr=\(currantLocation)&daddr=\(actionLocation)"
//        guard let url = URL(string: directionsURL) else {
//            return
//        }
//        if #available(iOS 10.0, *) {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        } else {
//            UIApplication.shared.openURL(url)
//        }
        let latituide = ((self.vahicaleDetail?.latitude ?? "0.0") as NSString).doubleValue
        let longitude = ((self.vahicaleDetail?.longitude ?? "0.0") as NSString).doubleValue
        let coordinate = CLLocationCoordinate2DMake(latituide,longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        mapItem.name = "Parking Spot"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    @IBAction func onSwitchConfirmed(_ sender: UIButton) {
        swichConfirm()
    }
    //MARK:-  Action Vehicl Detail API
    func actionVehicleDetail(){
        if Utility.isInternetAvailable(){
            let url = AUCTION_VEHICLE_DETAIL + "\(auctionId)"
            AuctionServices.shared.actionDetail(URLString: url){ (statusCode, response) in
                if let data = response.auctionVehicleDetailsResponse{
                    self.vahicaleDetail = data
                    self.setData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    //MARK:-  Action Vehicl Detail API
    func actionTransferPayment(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = imSpotRequest(auction_id: "\(auctionId)")
            AuctionServices.shared.actionTransferPayments(parameters:data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                self.openSucess()
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }

            }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:-  Swich to confirm
    func swichConfirm(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = imSpotRequest(auction_id: "\(auctionId)")
            AuctionServices.shared.switchBuyerConfirmSeller(parameters:data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                self.openSwichSucess()
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }

            }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    func openSucess(){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "You have successful paid for your parking spot. Please confirm that you have switched spaces by clicking the “Switch Confirmed” button below"
        vc.onClose = { [weak self] in
            self?.payWithCardButton.isHidden = true
            self?.swichConfirmedButton.isHidden = false
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func openSwichSucess(){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "Congratulations ! You completed your auction. You are now being redirected back to the homepage."
        vc.onClose = { [weak self] in
            self?.navigateSearchScreen()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func navigateSearchScreen(){
        for controller in self.navigationController!.viewControllers as Array {
              if controller is SearchScreen {
                  self.navigationController!.popToViewController(controller, animated: false)
                  break
              }
          }
    }
    
}
