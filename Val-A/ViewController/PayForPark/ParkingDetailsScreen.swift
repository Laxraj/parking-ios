//
//  ParkingDetailsScreen.swift
//  Val-A
//
//  Created by Nikunj on 03/03/21.
//

import UIKit
import PassKit
class ParkingDetailsScreen: UIViewController {
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var vehicleMakeLabel: UILabel!
    @IBOutlet weak var vehicleModel: UILabel!
    @IBOutlet weak var vehicleColor: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var zoneNameLabel: UILabel!
    @IBOutlet weak var durationNameLabel: UILabel!
    @IBOutlet weak var totalPaymentLabel: UILabel!
    @IBOutlet weak var zoneAddressLabel: UILabel!
    @IBOutlet weak var zoneIdLabel: UILabel!
    
    var price = 0.0
    var durationId = 0
    var zoneId: String = "0"
    var durationTitle = ""
    var locationName = ""
    var zoneAdress = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUserData()
    }
    func setUserData(){
        if let res = Utility.getUserInfoData(){
            self.yearLabel.text = "Year: \(res.vehicle_year_name ?? "")"
            self.vehicleMakeLabel.text = "Make: \(res.vehicle_make_name ?? "")"
            self.vehicleModel.text = "Model: \(res.vehicle_model_name ?? "")"
            self.vehicleColor.text = "Color: \(res.vehicle_color_name ?? "")"
            self.plateLabel.text = "Plate #: \(res.license_plate_no ?? "")"
        }
        
//        self.zoneNameLabel.text = locationName
//
        self.totalPaymentLabel.text = "$\(price)"
        
        
        self.zoneIdLabel.text  = "Zone id: "+(zoneId)
//        self.zoneNameLabel.text  = "Zone id: "+(locationName)
//        self.zoneNameLabel.text  = "Zone id: "+(locationName)
        self.zoneNameLabel.text = "Zone name: "+(locationName)
        self.zoneAddressLabel.text = "Zone address: "+(zoneAdress)
        self.durationNameLabel.text = "Duration: "+(durationTitle)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUserData()
    }
    

    @IBAction func onParkingDetail(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCardDetail(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "VehicleInformationScreen") as! VehicleInformationScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onApplePay(_ sender: UIButton) {
        let paymentNetwork = [ PKPaymentNetwork.amex,.discover, .masterCard,.visa]
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetwork){
            let request = PKPaymentRequest()
            request.merchantIdentifier = "merchant.com.vala.mobile"
            request.supportedNetworks = paymentNetwork
            request.supportedCountries = [ "US"]
//            request.merchantCapabilities = .capabilityEMV
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.countryCode = "US"
            request.currencyCode = "USD"
            request.paymentSummaryItems = [PKPaymentSummaryItem(label: "parking zone", amount: NSDecimalNumber(value: price))]
            let applePayvc = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayvc?.delegate = self
            self.present(applePayvc!, animated: true, completion: nil)
        }
        
    }
    @IBAction func onPayWithCard(_ sender: UIButton) {
        
        if let res = Utility.getUserInfoData(){
            if res.card_number == ""{
                Utility.showAlert(vc: self, message: "Please enter card number in your profile")
            }else{
                let vc = STORYBOARD.payForPark.instantiateViewController(withIdentifier: "PayForParkSummary") as! PayForParkSummary
                vc.modalPresentationStyle = .overCurrentContext
                vc.total = "\(self.price)"
                vc.zoneName = self.locationName
                vc.durationName = self.durationTitle
                vc.onClose = { [weak self] in
                    self?.payForParkAPI(transactionType: "1", paymentType: "1", transactionId: "")
                }
                self.present(vc, animated: true, completion: nil)
            }
        }
       
    }
    
    
    func payForParkAPI(transactionType:String,paymentType:String,transactionId:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = PayForParkRequest(duration_id: "\(self.durationId)", zone_id: self.zoneId, transaction_type: transactionType, payment_type: paymentType, price: "\(self.price)", payment_status: "success", transaction_id: transactionId)
            PayParkService.shared.payForPark(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
//                Utility.showAlert(vc: self, message: response.message ?? "")
                if let data = response.payForParkResponse{
                    print(data.toJSON())
                    self.goFurther(object: data)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    func goFurther(object: PayForParkResponse){
        let control = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "payForParkDescriptionScreen") as! payForParkDescriptionScreen
        control.obj = object
        self.navigationController?.pushViewController(control, animated: true)
    }
    
}
extension ParkingDetailsScreen : PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
//        payment.token
//        payment.billingContact?.phoneNumber
        
        /// 支付凭据，发给服务端进行验证支付是否真实有效
              let paymentToken = payment.token
              /// 账单信息
              let billingContact = payment.billingContact
              /// 送货信息
              let shippingContact = payment.shippingContact
              /// 送货方式
              let shippingMethod = payment.shippingMethod
              
              print("Religion: \(String(describing: billingContact))")
              print("Sending information: \(String(describing: shippingContact))")
              print("Delivery method: \(String(describing: shippingMethod))")
              print("Successful trade! ......\(paymentToken)")
               print(payment.token.transactionIdentifier)
        
        self.payForParkAPI(transactionType: "1", paymentType: "2", transactionId: payment.token.transactionIdentifier)
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: nil))
    }
}
