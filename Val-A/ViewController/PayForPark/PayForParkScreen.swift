//
//  PayForParkScreen.swift
//  Val-A
//
//  Created by iroid on 20/02/21.
//

import UIKit
import DropDown
class PayForParkScreen: UIViewController {

    @IBOutlet weak var privacyPolicyLabel: UILabel!
    @IBOutlet weak var payButton: dateSportButton!
    @IBOutlet weak var zoneLocationTextField: UITextField!
    @IBOutlet weak var parkingDurationTextField: UITextField!

    var zoneLocationArray: [ZoneListResponse] = []
    var parkingDurationArray: [ParkingTimeDurationResponse] = []
    var price =  0.0
    var durationId = 0
    var zoneId = "0"
    var zoneAdress = ""
    var tabVC: TabbarScreen?
    var isFindSpots = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPrivacyPolicyLabel()
        initialDetail()
    }
    
    func initialDetail(){
        getZoneLocationListAPI()
        getParkingTimeDurationAPI()
        self.zoneLocationTextField.text = zoneId
        self.zoneLocationTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.changePayButton()
    }
    
    func checkCardData() -> Bool{
        if Utility.getUserInfoData()?.card_number != nil &&  Utility.getUserInfoData()?.cvv != nil && Utility.getUserInfoData()?.expiration_date != nil{
            return true
        }
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isFindSpots{
            zoneLocationTextField.text = ""
        }
        parkingDurationTextField.text = ""
        self.getTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isFindSpots = false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let txt = textField.text else {
            return
        }
        if let zone = self.checkZone(zone: txt){
            self.zoneId = zone
        }else{
            self.zoneId = "0"
        }
        self.changePayButton()
    }
    
    func changePayButton(){
        if self.zoneId == "0"{
            self.payButton.backgroundColor = .darkGray
            self.payButton.isUserInteractionEnabled = false
        }else{
            self.payButton.backgroundColor = Utility.getUIcolorfromHex(hex: "3F68FF")
            self.payButton.isUserInteractionEnabled = true
        }
    }
    
    func checkZone(zone: String) -> String?{
        var zoneArr: [String] = []
        for i in self.zoneLocationArray{
            if let id = i.id{
                zoneArr.append(id)
            }
        }
        let filteredIndices = zoneLocationArray.indices.filter {zoneLocationArray[$0].id == zone}
         
        if let zone = zoneArr.first(where: {$0 == zone}){
            print(zone)
            self.zoneAdress = zoneLocationArray[filteredIndices.first ?? 0].zone_address ?? ""
            
            return zone
        }
        return nil
    }

    func setPrivacyPolicyLabel(){
        let string  = "By continuing you are agreeing to Val-A’s, Terms of Use, and confirm that you have read and agree with Val-A’s, Privacy Policy."
        let attributedString = NSMutableAttributedString(string: string)
        let termsRange = (string as NSString).range(of: "Terms of Use")
        let privacyPolicyRange = (string as NSString).range(of: "Privacy Policy.")
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 15)!,NSAttributedString.Key.underlineColor : UIColor.white,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: termsRange)
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 15)!,NSAttributedString.Key.underlineColor : UIColor.white,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: privacyPolicyRange)
        
        self.privacyPolicyLabel.attributedText = attributedString
        self.privacyPolicyLabel.isUserInteractionEnabled = true
        self.privacyPolicyLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
    }

    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = privacyPolicyLabel.attributedText?.string else {
            return
        }

        if let range = text.range(of: NSLocalizedString("Terms of Use", comment: "terms")),
           recognizer.didTapAttributedTextInLabel(label: self.privacyPolicyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: teamOfUseLink)
        } else if let range = text.range(of: NSLocalizedString("Privacy Policy.", comment: "privacy")),
            recognizer.didTapAttributedTextInLabel(label: self.privacyPolicyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: privacyLink)
        }
    }
    func openUrl(strUrl:String){
        if let url = URL(string: strUrl) {
            UIApplication.shared.open(url)
        }
    }
    @IBAction func onZoneLocation(_ sender: UIButton) {
        
//        var arr: [String] = []
//        for i in self.zoneLocationArray{
//            if let item = i.name{
//                arr.append(item)
//            }
//        }
//        displayDropDown(array: arr, tag: 0, view: self.zoneLocationTextField)
    }
    
    @IBAction func onParkingDuration(_ sender: UIButton) {
        var arr: [String] = []
        for i in self.parkingDurationArray{
            if let item = i.durations{
                arr.append(item)
            }
        }
        displayDropDown(array: arr, tag: 1, view: self.parkingDurationTextField)
    }
    
    @IBAction func onPay(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            guard self.checkCardData() else {
                Utility.showAlert(vc: self, message: "Please enter your card")
                return
            }
//            self.loginAPI()
            
            if let val = self.parkingDurationArray.first(where: {$0.durations == self.parkingDurationTextField.text}){
                price = val.price ?? 0.0
                durationId = val.id ?? 0
            }
            var zone : String = ""
            if let val = self.zoneLocationArray.first(where: {$0.id == self.zoneLocationTextField.text}){
                zone = val.name ?? ""
            }
            
            let vc = STORYBOARD.payForPark.instantiateViewController(withIdentifier: "ParkingDetailsScreen") as! ParkingDetailsScreen
            vc.price = price
            vc.locationName = zone//self.zoneLocationTextField.text ?? ""
            vc.durationTitle = self.parkingDurationTextField.text ?? ""
            vc.durationId = self.durationId
            vc.zoneId = self.zoneId
            vc.zoneAdress = self.zoneAdress
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func checkValidation() -> String?{
        if self.zoneLocationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter zone location"
        }else if self.parkingDurationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select duration"
        }
        return nil
    }
    
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
                self?.zoneLocationTextField.text = item
                dropDown.hide()
            default:
                self?.parkingDurationTextField.text = item
                dropDown.hide()
            }
        }
    }
    
    //MARK:-  Zone Location API
    func getZoneLocationListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            PayParkService.shared.getZoneList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.zoneLocationArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:-  Zone Location API
    func getParkingTimeDurationAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            PayParkService.shared.getParkingTimeDurationList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.parkingDurationArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- GET TIMER
    func getTimer(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            PayParkService.shared.getTimer { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let data = response.payForParkResponse{
                    if data.is_timmer == 1{
                        self?.goFurther(object: data)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func goFurther(object: PayForParkResponse){
        let control = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "payForParkDescriptionScreen") as! payForParkDescriptionScreen
        control.obj = object
        control.tabVC = self.tabVC
        self.navigationController?.pushViewController(control, animated: false)
    }
}
