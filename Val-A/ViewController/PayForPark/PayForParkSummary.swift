//
//  PayForParkSummary.swift
//  Val-A
//
//  Created by iroid on 05/03/21.
//

import UIKit

class PayForParkSummary: UIViewController {
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var vehicleMakeLabel: UILabel!
    @IBOutlet weak var vehicleModel: UILabel!
    @IBOutlet weak var vehicleColor: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var zoneNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    var onClose: (() -> Void)?
    
    var total: String?
    var zoneName: String?
    var durationName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUserData()
    }
    func setUserData(){
        if let res = Utility.getUserInfoData(){
            self.yearLabel.text = "Year: \(res.vehicle_year_name ?? "")"
            self.vehicleMakeLabel.text = "Make: \(res.vehicle_make_name ?? "")"
            self.vehicleModel.text = "Model: \(res.vehicle_model_name ?? "")"
            self.vehicleColor.text = "Color: \(res.vehicle_color_name ?? "")"
            self.plateLabel.text = "Plate #: \(res.license_plate_no ?? "")"
            self.totalLabel.text = "Total :- "+(total ?? "0")+"$"
            self.zoneNameLabel.text = self.zoneName
            self.durationLabel.text = "Duration :- "+(self.durationName ?? "")
            self.cardNumberLabel.text = "Last 4 of Card Number : ....\(res.card_number?.suffix(4) ?? "")"
        }
    }

    @IBAction func onCancel(_ sender: UIButton) {
        self.dismiss(animated:false, completion: nil)
    }
    
    @IBAction func onPark(_ sender: UIButton) {
        self.dismiss(animated: false, completion: { [weak self] in
            self?.onClose?()
        })
    }
}
