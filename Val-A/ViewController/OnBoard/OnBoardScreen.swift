//
//  OnBoardScreen.swift
//  Val-A
//
//  Created by Nikunj on 02/04/21.
//

import UIKit

class OnBoardScreen: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var getStartedButton: UIButton!
    var itemArray : [OnBoardRequest] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPageControl()
        self.collectionView.register(UINib(nibName: "OnBoardCell", bundle: nil), forCellWithReuseIdentifier: "OnBoardCell")
        self.setArray()
        self.pageControl.numberOfPages = self.itemArray.count
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.getStartedButton.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    func setArray(){
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_0"), description: "Val-A is a peer-to-peer parking marketplace that allows you to find convenient parking spots and make money all at the same time."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_1"), description: "Val-A offers 2 different features. Pay for Park and Parking Spot Auctioning.Both features are great ways for users to maximize their convenience when parking."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_2"), description: "With Pay For Park you can pay for your parking meter digitally. Just type in your zone location and select your parking duration and you’re all set."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_3"), description: "With Parking Spot Auctioning, users can choose to be either the buyer or the seller. It just all depends which you choose to be at the moment."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_4"), description: "As a seller, you must first secure your parking spot. Before exiting your vehicle, place your spot up for auction so that we get your vehicles location."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_5"), description: "The buyer searches for available auctions near their intended destination and places a bid until they have won an auction."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_6"), description: "When the auction ends, both buyer and seller exchange their vehicle information and get a link that directs them to the parking spot to complete the exchange."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_7"), description: "Now you can make money just by having a convenient parking spot or pay to conveniently find one. Either way, Everyone wins!"))
    }

    
    
    @IBAction func onGetStarted(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "isOnBoarding")
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginOptionScreen") as! LoginOptionScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
  
    func setPageControl(){
        self.pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 2, y: 2)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)

//        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
//        if let ip = collectionView.indexPathForItem(at: center) {
//            self.pageControl.currentPage = ip.row
//        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
//        if let ip = collectionView.indexPathForItem(at: center) {
//            self.pageControl.currentPage = ip.row
            if itemArray.count - 1 == pageControl.currentPage{
                getStartedButton.isHidden = false
                pageControl.isHidden = true
            }else{
                pageControl.isHidden = false
                getStartedButton.isHidden = true
            }

//        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)

    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if itemArray.count - 1 == pageControl.currentPage{
            getStartedButton.isHidden = false
            pageControl.isHidden = true
        }else{
            pageControl.isHidden = false
            getStartedButton.isHidden = true
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

}
extension OnBoardScreen: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardCell", for: indexPath) as! OnBoardCell
        cell.item = self.itemArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: self.collectionView.frame.size.height)
    }
}
