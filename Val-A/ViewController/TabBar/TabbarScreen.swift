//
//  TabbarScreen.swift
//  Val-A
//
//  Created by iroid on 20/02/21.
//

import UIKit

class TabbarScreen: UITabBarController {

    
    var isFromNotification = false
    var type = 0
    var zoneID: String = "0"
    var isFromSellMySpot = false
    var isPayForPark = false
    var zoneAddress = ""
   // var viewControllerToSelect: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setUpTabBar()
        self.getUserData()
    }
    
    func getUserData(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            LoginService.shared.getUserInfo { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    Utility.setUserInfoData(data: res.toJSON())
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    

    func setUpTabBar(){
        tabBar.barTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tabBar.isTranslucent = false
       // self.delegate = self

        tabBar.tintColor = #colorLiteral(red: 0.3189409077, green: 0.6059766412, blue: 0.9667704701, alpha: 1)
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0.3189409077, green: 0.6059766412, blue: 0.9667704701, alpha: 1)
//        UITabBar.appearance().shadowImage = UIImage.colorForNavBar(color: .red)
        
        if #available(iOS 15.0, *) {
           let appearance = UITabBarAppearance()
           appearance.configureWithOpaqueBackground()
           appearance.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           
           tabBar.standardAppearance = appearance
           tabBar.scrollEdgeAppearance = tabBar.standardAppearance
        }
        
        let searchStoryBoard = UIStoryboard(name: "Search", bundle:   nil)
        let searchScreen = searchStoryBoard.instantiateViewController(withIdentifier: "SearchScreen") as! SearchScreen
        let searchNavigationController = UINavigationController(rootViewController: searchScreen)
        searchNavigationController.isNavigationBarHidden = true
        
        let payForParkStoryBoard = UIStoryboard(name: "PayForPark", bundle:   nil)
        let payForParkScreen = payForParkStoryBoard.instantiateViewController(withIdentifier: "PayForParkScreen") as! PayForParkScreen
        let payForParkavigationController = UINavigationController(rootViewController: payForParkScreen)
        payForParkScreen.tabVC = self
        payForParkScreen.isFindSpots = isPayForPark
        payForParkScreen.zoneId = self.zoneID
        payForParkScreen.zoneAdress = self.zoneAddress
        payForParkavigationController.isNavigationBarHidden = true
        
        
        let auctionStoryBoard = UIStoryboard(name: "Auction", bundle:   nil)
        let auctionScreen = auctionStoryBoard.instantiateViewController(withIdentifier: "AuctionScreen") as! AuctionScreen
        let auctionvigationController = UINavigationController(rootViewController: auctionScreen)
        auctionScreen.zoneId = self.zoneID
        auctionvigationController.isNavigationBarHidden = true
        
        
        let profileStoryBoard = UIStoryboard(name: "Profile", bundle:   nil)
        let profileScreen = profileStoryBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen
        let profileNavigationController = UINavigationController(rootViewController: profileScreen)
        profileNavigationController.isNavigationBarHidden = true
        
        viewControllers = [searchNavigationController,payForParkavigationController,auctionvigationController,profileNavigationController]
        
        self.tabBar.items![0].image = UIImage(named: "tabbar_Search_icon")
        self.tabBar.items![1].image = UIImage(named: "tabbar_pay_icon")
        self.tabBar.items![2].image = UIImage(named: "tabbar_auction_icon")
        self.tabBar.items![3].image = UIImage(named: "tabbar_user_icon")
        self.tabBar.items![0].title = "Find Spots"
        self.tabBar.items![1].title = "Pay For Park"
        self.tabBar.items![2].title = "Auctions"
        self.tabBar.items![3].title = "Profile"
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                self.tabBar.items?[i].imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
            }
        }
        
        if type == 1 || type == 4 ||  type == 5 || type == 8{
            self.selectedIndex = 2
        }else if type == 2{
            self.selectedIndex = 1
        }else if type == 3{
            self.selectedIndex = 0
        }
        if isFromSellMySpot{
            self.selectedIndex = 2
        }else if isPayForPark{
            self.selectedIndex = 1
        }
    }
   
    func showLeavingAlert() {
        let leavingAlert = UIAlertController(title: APP_NAME, message: "Comming soon", preferredStyle: .alert)

        let saveAction = UIAlertAction(title: "OK", style: .default) { (action) in
//            let delayTime = dispatch_time(dispatch_time_t(DispatchTime.now()), Int64(1 * Double(NSEC_PER_SEC)))
//              dispatch_after(delayTime, dispatch_get_main_queue()) {
                  // switch viewcontroller after one second delay (faked save action)
                  //self.performSwitch()
//              }
          }
          leavingAlert.addAction(saveAction)

//        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action) in
//              // switch viewcontroller immediately
//              self.performSwitch()
//          }
//          leavingAlert.addAction(cancelAction)

        present(leavingAlert, animated: true, completion: nil)
      }
    
//    func performSwitch() {
//            if let viewControllerToSelect = viewControllerToSelect {
//                // switch viewcontroller immediately
//                selectedViewController = viewControllerToSelect
//                // reset reference
//                self.viewControllerToSelect = nil
//            }
//        }
    
}
//extension TabbarScreen: UITabBarControllerDelegate {
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        if let navigationController = selectedViewController as? UINavigationController, let _ = navigationController.visibleViewController as? AuctionScreen {
//            // save a reference to the viewcontroller the user wants to switch to
//
//            // present the alert
//            showLeavingAlert()
//
//            // return false so that the tabs do not get switched immediately
//            return false
//        }
//
//        return true
//    }
//}
