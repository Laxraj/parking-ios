//
//  WhatDoYouDriveScreen.swift
//  Val-A
//
//  Created by iroid on 08/02/21.
//

import UIKit
import DropDown

class WhatDoYouDriveScreen: UIViewController {

    @IBOutlet weak var vehicleView: dateSportView!
    @IBOutlet weak var vehicleModelView: dateSportView!
    @IBOutlet weak var vehicleYearView: dateSportView!
    @IBOutlet weak var vehicleColorView: dateSportView!
    @IBOutlet weak var vehicleMakeTextField: UITextField!
    @IBOutlet weak var vehicleModelTextField: UITextField!
    @IBOutlet weak var vehicleYearTextField: UITextField!
    @IBOutlet weak var vehicleColorTextField: UITextField!
    @IBOutlet weak var licensePlateTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var vehicleMakeArray: [VehicleMakeResponse] = []
    var vehicleModelArray: [VehicleModelResponse] = []
    var vehicleYearArray: [VehicleYearResponse] = []
    var vehicleColorArray: [VehicleColorResponse] = []
    var profileImageData: Data?
    
    var signUpObj : SignUpRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getVehicleListAPI()
        self.getVehicleYearListAPI()
//        self.getVehicleModelListAPI()
        self.getVehicleColorListAPI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onVehicleMake(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleMakeArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 0, view: self.vehicleView)
    }
    
    @IBAction func onVehicleModel(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleModelArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 1, view: self.vehicleModelView)
    }
    
    @IBAction func onVahicleYear(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleYearArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 2, view: self.vehicleYearView)
    }
    
    @IBAction func onVehicleColor(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleColorArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 3, view: self.vehicleColorView)
    }
    
    @IBAction func onUpload(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
        
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
                self?.vehicleMakeTextField.text = item
                self?.vehicleModelTextField.text = nil
                self?.vehicleModelArray = self?.vehicleMakeArray[index].models ?? []
                dropDown.hide()
            case 1:
                self?.vehicleModelTextField.text = item
                dropDown.hide()
            case 2:
                self?.vehicleYearTextField.text = item
                dropDown.hide()
            default:
                self?.vehicleColorTextField.text = item
                dropDown.hide()
            }
        }
    }
    
    //MARK:- Vehicle List API
    func getVehicleListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleList { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleMakeArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Model API
    func getVehicleModelListAPI(vehicleMakeID: Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleModelList(vehicleMakeId: vehicleMakeID, success: { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleModelArray = res
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Year API
    func getVehicleYearListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleYearList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleYearArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Color API
    func getVehicleColorListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleColorList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleColorArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- CHECK VALIDATION
    func checkValidation() -> String?{
        if profileImageData == nil{
            return "Please upload profile photo"
        }else if self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle make"
        }else if self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle model"
        }else if self.vehicleYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle year"
        }else if self.vehicleColorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle color"
        }
        return nil
    }
    
    @IBAction func onNext(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            var vehicleMakeID: String?
            var vehicleModelID: String?
            var vehicleYearID: String?
            var vehicleColorID: String?

            if let val = self.vehicleMakeArray.first(where: {$0.name == self.vehicleMakeTextField.text}){
                vehicleMakeID = "\(val.vehicle_make_id ?? 0)"
            }
            if let val = self.vehicleModelArray.first(where: {$0.name == self.vehicleModelTextField.text}){
                vehicleModelID = "\(val.id ?? 0)"
            }
            if let val = self.vehicleYearArray.first(where: {$0.name == self.vehicleYearTextField.text}){
                vehicleYearID = "\(val.vehicle_year_id ?? 0)"
            }
            
            if let val = self.vehicleColorArray.first(where: {$0.name == self.vehicleColorTextField.text}){
                vehicleColorID = "\(val.vehicle_color_id ?? 0)"
            }
            
            
            self.registerAPI(makeID: vehicleMakeID, modelID: vehicleModelID, yearID: vehicleYearID, colorID: vehicleColorID)
        }
    }
    
    func callRegisterForPushAPI(){
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.registerForPush()
    }
    
    //MARK:- REGISTER USER
    func registerAPI(makeID: String?,modelID: String?,yearID: String?,colorID: String?){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
//            self.signUpObj?.phone
            let data = SignUpRequest(first_name: self.signUpObj?.first_name, last_name: self.signUpObj?.last_name, phone: self.signUpObj?.phone, password: self.signUpObj?.password, vehicle_make: makeID, vehicle_model: modelID, vehicle_year: yearID, vehicle_color: colorID, license_plate_no: self.licensePlateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),provider_id:signUpObj?.provider_id,provider_type:signUpObj?.provider_type, country_code: self.signUpObj?.country_code)
            LoginService.shared.registerUser(parameters: data.toJSON(), image: self.profileImageData) { [weak self]  (statusCode, response) in
                guard let strongSelf = self else { return }
                Utility.hideIndicator()
//                Utility.showAlert(vc: strongSelf, message: response.message ?? "")
                if let res = response?.loginResponse{
                    print(res.toJSON())
                    Utility.saveUserData(data: res.toJSON())
                    LoginService.shared.isFirstTime(userID: res.user_id ?? 0) { (statusCode, response) in
                        print(response.toJSON())
                    } failure: { (error) in
                        print(error)
                    }
                    self?.callRegisterForPushAPI()
                    Utility.setTabRoot()
//
//                    let storyBoard = UIStoryboard(name: "Login", bundle: nil)
//                    let control = storyBoard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
//                    strongSelf.navigationController?.pushViewController(control, animated: true)
                }
            } failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: strongSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- SET IMAGEVIEW
extension WhatDoYouDriveScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        profileImageView.image = pickedImage
        self.dismiss(animated: true, completion: { () -> Void in
            self.profileImageData = pickedImage.resized()?.pngData()
        })
    }
    
    func openCamera()
    {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
}
