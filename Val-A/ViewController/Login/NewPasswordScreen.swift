//
//  NewPasswordScreen.swift
//  Val-A
//
//  Created by iroid on 11/02/21.
//

import UIKit

class NewPasswordScreen: UIViewController {

    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var phone: String?
    var countryCode: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPrivacyPolicyLabel()
        // Do any additional setup after loading the view.
    }
    func setPrivacyPolicyLabel(){
        let string  = "By continuing you are agreeing to Val-A’s, Terms of Use, and confirm that you have read and agree with Val-A’s, Privacy Policy."
        let attributedString = NSMutableAttributedString(string: string)
        let termsRange = (string as NSString).range(of: "Terms of Use")
        let privacyPolicyRange = (string as NSString).range(of: "Privacy Policy.")
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 12)!,NSAttributedString.Key.underlineColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: termsRange)
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 12)!,NSAttributedString.Key.underlineColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: privacyPolicyRange)
        self.privacyLabel.attributedText = attributedString
        self.privacyLabel.isUserInteractionEnabled = true
        self.privacyLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
    }
    
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = privacyLabel.attributedText?.string else {
            return
        }

        if let range = text.range(of: NSLocalizedString("Terms of Use", comment: "terms")),
           recognizer.didTapAttributedTextInLabel(label: self.privacyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: teamOfUseLink)
        } else if let range = text.range(of: NSLocalizedString("Privacy Policy.", comment: "privacy")),
            recognizer.didTapAttributedTextInLabel(label: self.privacyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: privacyLink)
        }
    }

    func openUrl(strUrl:String){
        if let url = URL(string: strUrl) {
            UIApplication.shared.open(url)
        }
    }
   
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- CHECK VALIDATION
    func checkValidation() -> String?{
        if self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter new password"
        }else if self.confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter confirm password"
        }else if self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != self.confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            return "New password & confirm password does not match"
        }
        return nil
    }
    
    @IBAction func onReset(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.resetPasswordAPI()
        }
    }
    
    @IBAction func onCreateAccount(_ sender: UIButton) {
       let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(control, animated: false)
        
    }
    
    //MARK:- Reset Password API
    func resetPasswordAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ForgotPasswordRequest(phone: self.phone, password: self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), password_confirmation: self.confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), country_code: countryCode)
            LoginService.shared.resetPassword(parameters: data.toJSON(), success: { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                guard let stronSelf = self else { return }
//                Utility.showAlert(vc: stronSelf, message: response.message ?? "")
                for controller in stronSelf.navigationController!.viewControllers as Array {
                    if controller is LoginScreen {
                        stronSelf.navigationController?.popToViewController(controller, animated: true)
                        break
                    }
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
