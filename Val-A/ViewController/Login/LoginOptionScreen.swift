//
//  LoginOptionScreen.swift
//  Val-A
//
//  Created by iroid on 08/02/21.
//

import UIKit
import GoogleSignIn

class LoginOptionScreen: UIViewController {

    @IBOutlet weak var privacyLabel: UILabel!
    var signUpObj : SignUpRequest?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.setPrivacyPolicyLabel()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func onSignInWithPhone(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onSignUp(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "WhoAreYouScreen") as! WhoAreYouScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    func setPrivacyPolicyLabel(){
        let string  = "By continuing you are agreeing to Val-A’s, Terms of Use, and confirm that you have read and agree with Val-A’s, Privacy Policy."
        let attributedString = NSMutableAttributedString(string: string)
        let termsRange = (string as NSString).range(of: "Terms of Use")
        let privacyPolicyRange = (string as NSString).range(of: "Privacy Policy.")
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 15)!,NSAttributedString.Key.underlineColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: termsRange)
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 15)!,NSAttributedString.Key.underlineColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: privacyPolicyRange)
        self.privacyLabel.attributedText = attributedString
        self.privacyLabel.isUserInteractionEnabled = true
        self.privacyLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = privacyLabel.attributedText?.string else {
            return
        }

        if let range = text.range(of: NSLocalizedString("Terms of Use", comment: "terms")),
           recognizer.didTapAttributedTextInLabel(label: self.privacyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: teamOfUseLink)
        } else if let range = text.range(of: NSLocalizedString("Privacy Policy.", comment: "privacy")),
            recognizer.didTapAttributedTextInLabel(label: self.privacyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: privacyLink)
        }
    }
    
    func openUrl(strUrl:String){
        if let url = URL(string: strUrl) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func noAppleLogin(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let loginManager = AppleLoginManager()
            loginManager.mainView = self.view
            loginManager.handleAuthorizationAppleIDButtonPress()
            loginManager.delegate = self
        } else {
            Utility.showAlert(vc: self, message: "Apple login not supported below iOS 13 OS.")
        }
    }
    
    @IBAction func onGoogleLogin(_ sender: Any) {
        let loginManager = GoogleLoginManager()
        loginManager.handleGoogleLoginButtonTap(viewController: self)
        loginManager.delegate = self
    }
    
    @IBAction func onFacebook(_ sender: Any) {
        let loginManager = FacebookLoginManager()
        loginManager.handleFacebookLoginButtonTap(viewController: self)
        loginManager.delegate = self
    }
    
    
    func fetchData(firtName:String,lastName:String,phoneNumber:String,providerID:String,providerType:String){
        signUpObj = SignUpRequest(first_name: firtName, last_name: lastName, phone: nil, password: phoneNumber, vehicle_make: nil, vehicle_model: nil, vehicle_year: nil, vehicle_color: nil, license_plate_no: nil, provider_id: providerID, provider_type: providerType, country_code: nil)
        
    }
    func callRegisterForPushAPI(){
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.registerForPush()
    }
 
    
    //MARK:- Social Login API
    func socialLogin(data: [String:Any]){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.socialLogin(parameters: data) { [weak self] (statusCode, response) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                if let res = response{
                    print(response.toJSON())
                    let storyBoard = UIStoryboard(name: "Login", bundle: nil)
                    let control = storyBoard.instantiateViewController(withIdentifier: "WhoAreYouScreen") as! WhoAreYouScreen
                    control.signUpObj = stronSelf.signUpObj
                    if response.is_login_first_time == true || response.is_login_first_time == nil{
                        control.isFromSocial = true
                        stronSelf.navigationController?.pushViewController(control, animated: true)
                    }else{
                        //MARK:- OLD USER
//                        Utility.hideIndicator()
                        if let res = response.loginResponse{
                            self?.callRegisterForPushAPI()
                            Utility.saveUserData(data: res.toJSON())
                            Utility.setTabRoot()
                        }
//                        Utility.showAlert(vc: stronSelf, message: "User login Successfully")
                    }
//                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: "User login Successfully")
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- Google Login Delegate
extension LoginOptionScreen: googleLoginManagerDelegate{
    func onGoogleLoginSuccess(user: GIDGoogleUser) {
        fetchData(firtName: user.profile.givenName, lastName: user.profile.familyName, phoneNumber: "", providerID: user.userID, providerType: GOOGLE)
        let  data = SocialLoginRequest(provider_id: user.userID, provider_type: GOOGLE, token: nil)
        self.socialLogin(data: data.toJSON())
    }
    
    func onGoogleLoginFailure(error: NSError) {
        Utility.showAlert(vc: self, message: error.localizedDescription)
    }
}
//MARK:- Apple login Delegate
extension LoginOptionScreen: AppleLoginManagerDelegate{
    func onSuccess(result: NSDictionary) {
        fetchData(firtName: (result.value(forKey: USERNAME) as? String) ?? "", lastName: "", phoneNumber: "", providerID: (result.value(forKey: "userid") as? String) ?? "", providerType: APPLE)
        let data = SocialLoginRequest(provider_id: result.value(forKey: "userid") as? String, provider_type: APPLE, token: nil)
        self.socialLogin(data: data.toJSON())
    }
    
    func onFailure(error: NSError) {
        Utility.showAlert(vc: self, message: error.localizedDescription)
    }
}
//MARK:- Facebook Login Delegate
extension LoginOptionScreen: FacebookLoginManagerDelegate{
    func onFacebookLoginSuccess(user: FacebookUserDetail) {
        fetchData(firtName: user.socialName ?? "", lastName: "", phoneNumber: "", providerID: user.socialId ?? "", providerType: FACEBOOK)
        let data = SocialLoginRequest(provider_id: user.socialId ?? "", provider_type: FACEBOOK, token: nil)
        self.socialLogin(data: data.toJSON())
    }
    
    func onFacebookLoginFailure(error: NSError) {
        print(error.localizedDescription)
        Utility.showAlert(vc: self, message: error.localizedDescription)
    }
}
