//
//  WhoAreYouScreen.swift
//  Val-A
//
//  Created by iroid on 08/02/21.
//

import UIKit
import GCCountryPicker

class WhoAreYouScreen: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordViewHeight: NSLayoutConstraint!
    var signUpObj : SignUpRequest?
    var isFromSocial = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", "US"))
        self.setupCallingCode()

        initialDetail()
    }
    func initialDetail(){
        if isFromSocial{
            passwordView.isHidden = true
            passwordViewHeight.constant = 0
            firstNameTextField.text = signUpObj?.first_name
            lastNameTextField.text = signUpObj?.last_name
        }
    }
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            codeLabel.text = code
            countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", countryCode))
        }
    }

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onNext(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.goFurther()
        }
       
    }
    @IBAction func onCode(_ sender: Any) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func goFurther(){
        let data = SignUpRequest(first_name: self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), last_name: self.lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), phone: (self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""), password: self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), vehicle_make: nil, vehicle_model: nil, vehicle_year: nil, vehicle_color: nil, license_plate_no: nil,provider_id:signUpObj?.provider_id,provider_type:signUpObj?.provider_type, country_code: (self.codeLabel.text ?? ""))
        
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "OtpScreen") as! OtpScreen
        control.screenType = OtpScreenNavigationType.whoAreYou.rawValue
        control.signUpObj = data
        control.countryCode = (self.codeLabel.text ?? "")
        control.mobileNumber = (self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        self.navigationController?.pushViewController(control, animated: true)
        
        return
     
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkValidation() -> String?{
        if self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter first name"
        }else if self.lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter last name"
        }else if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone number"
        }else if !isFromSocial && self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter password"
        }else if !isFromSocial && self.passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count < 8{
            return "Please enter valid password"
        }
    
        return nil
    }
}
//MARK:- GCCountryPicker Delegate & DataSource
extension WhoAreYouScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
         self.dismiss(animated: true, completion: nil)
     }
     
     func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
         self.codeLabel.text = country.callingCode
         countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", country.countryCode))
         self.dismiss(animated: true, completion: nil)
     }
}
