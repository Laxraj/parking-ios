//
//  OtpScreen.swift
//  Val-A
//
//  Created by iroid on 11/02/21.
//

import UIKit
import FirebaseAuth

class OtpScreen: UIViewController {
    
    @IBOutlet weak var otpTextField1: UITextField!
    @IBOutlet weak var otpTextField2: UITextField!
    @IBOutlet weak var otpTextField3: UITextField!
    @IBOutlet weak var otpTextField4: UITextField!
    @IBOutlet weak var otpTextField5: UITextField!
    @IBOutlet weak var otpTextField6: UITextField!
    
    @IBOutlet weak var pwdResetLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var resendCodeView: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    var mobileNumber: String?
    var countryCode: String?
    var timer = Timer()
    var secondsRemaining = 60
    var signUpObj : SignUpRequest?
    var screenType = 0
    var onClose: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        switch screenType {
        case OtpScreenNavigationType.whoAreYou.rawValue, OtpScreenNavigationType.editProfile.rawValue:
            self.pwdResetLabel.text = "Enter the verification code we just sent you on your phone number \(countryCode ?? "")" + " " + "\(mobileNumber ?? "")"
            self.titleLabel.text = "Verification"
//            self.titleLabel.isHidden = true
            self.logoutButton.isHidden = true
        case OtpScreenNavigationType.forgotPassword.rawValue:
            self.pwdResetLabel.text = "A reset link has been sent to your phone number \n to help you log into your account. Please enter the code \nsent to your device to complete the password reset.\n Thank you!"
            self.titleLabel.isHidden = false
            self.logoutButton.isHidden = true
        case OtpScreenNavigationType.afterLogin.rawValue,OtpScreenNavigationType.fromHome.rawValue:
            self.pwdResetLabel.text = "Enter the verification code we just sent you on your phone number \(countryCode ?? "")" + " " + "\(mobileNumber ?? "")"
            self.titleLabel.text = "Verification"
            self.backButton.isHidden = true
            self.logoutButton.isHidden = false
        default:
            self.pwdResetLabel.text = "Enter the verification code we just sent you on your phone number \(countryCode ?? "")" + " " + "\(mobileNumber ?? "")"
            self.titleLabel.text = "Verification"
            self.logoutButton.isHidden = true
        }
        
//        if self.isFromWhoAreYouScreen{
//            self.pwdResetLabel.text = nil
//            self.titleLabel.isHidden = true
//        }else{
//            self.pwdResetLabel.text = "A reset link has been sent to your phone number \n to help you log into your account. Please enter the code \nsent to your device to complete the password reset.\n Thank you!"
//            self.titleLabel.isHidden = false
//        }
        self.initialDetail()
//        mobileNumber = "+91 7802878898"
        otpSend()
        
        // Do any additional setup after loading the view.
    }
    @objc func otpSend(){
        self.timerStart()
        let mobileNumberr = "\(countryCode ?? "")" + " " + "\(mobileNumber ?? "")"
        if Utility.isInternetAvailable(){
            if let mobileNo = self.mobileNumber{
                PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumberr, uiDelegate: nil) { (verificationID, error) in
                    if let error = error {
                        //                        print(error.localizedDescription)
                        //                        self.setResendOTPText()
                        if error.localizedDescription == "TOO_SHORT"{
                            Utility.showAlert(vc: self, message: "Invalid Mobile Number")
                        }else{
                            print(error.localizedDescription)
                            Utility.showAlert(vc: self, message: error.localizedDescription)
                        }
                        return
                    }
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                }
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
        
        //        self.otpTimerLabel.text = "Having trouble? Request a new OTP in 00:30"
    }
    func initialDetail(){
        otpTextField1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
    }
    
    func timerStart(){
        self.resendCodeView.isHidden = true
        self.timerLabel.isHidden = false
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
            if self.secondsRemaining > 0 {
                self.setOTPSecondText(second: self.secondsRemaining)
                self.secondsRemaining -= 1
            } else {
                self.timerLabel.isHidden = true
                self.timer.invalidate()
                self.setResendOTPText()
            }
        }
    }
    
    func setResendOTPText(){
        self.secondsRemaining = 60
        self.resendCodeView.isHidden = false
//        self.timer.invalidate()
//        let string = "Having trouble? Request a new OTP Resend"
        //        let attributedString = NSMutableAttributedString(string: string)
        //        let secondRange = (string as NSString).range(of: "Resend")
        //        attributedString.addAttributes([NSAttributedString.Key.font : isiPad ?  UIFont(name: "Poppins-Bold", size: 17)! :  UIFont(name: "Poppins-Bold", size: 14)!,NSAttributedString.Key.foregroundColor:  Utility.getUIcolorfromHex(hex: "E85A4F")], range: secondRange)
        //        self.otpTimerLabel.attributedText = attributedString
        //        self.otpTimerLabel.isUserInteractionEnabled = true
        //        self.otpTimerLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.otpSend)))
    }
    func setOTPSecondText(second: Int){
        var seconds: String = "\(second)"
        if (1...9).contains(self.secondsRemaining){
            seconds = "0"+seconds
        }
        let string = "Having trouble? Request a new OTP in 00:\(seconds)"
        self.timerLabel.text = string
       
    }
    @IBAction func onResend(_ sender: UIButton) {
        self.otpSend()
        
    }
    @IBAction func onNext(_ sender: UIButton) {
        checkOTP()
    }
    
    
    @IBAction func onLogout(_ sender: UIButton) {
        self.showLogOutOptions()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showLogOutOptions() {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        self.logOutApi()
                                      }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goFurther(){
        switch screenType {
        case OtpScreenNavigationType.whoAreYou.rawValue:
            let storyBoard = UIStoryboard(name: "Login", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "WhatDoYouDriveScreen") as! WhatDoYouDriveScreen
            control.signUpObj = signUpObj
            self.navigationController?.pushViewController(control, animated: true)
            
        case OtpScreenNavigationType.afterLogin.rawValue,OtpScreenNavigationType.fromHome.rawValue:
            let userData = Utility.getUserData()
            userData?.isOTPVerify = true
            Utility.saveUserData(data: userData?.toJSON() ?? [:])
            Utility.setTabRoot()
        case OtpScreenNavigationType.forgotPassword.rawValue:
            let storyBoard = UIStoryboard(name: "Login", bundle: nil)
            let control = storyBoard.instantiateViewController(withIdentifier: "NewPasswordScreen") as! NewPasswordScreen
            control.phone = self.mobileNumber
            control.countryCode = self.countryCode
            self.navigationController?.pushViewController(control, animated: true)
        case OtpScreenNavigationType.editProfile.rawValue:
            self.onClose?()
            self.navigationController?.popViewController(animated: true)
        default:
            self.pwdResetLabel.text = nil
            self.titleLabel.isHidden = true
        }
        
        
//        if isFromWhoAreYouScreen{
//            let storyBoard = UIStoryboard(name: "Login", bundle: nil)
//            let control = storyBoard.instantiateViewController(withIdentifier: "WhatDoYouDriveScreen") as! WhatDoYouDriveScreen
//            control.signUpObj = signUpObj
//            self.navigationController?.pushViewController(control, animated: true)
//        }else{
//            let storyBoard = UIStoryboard(name: "Login", bundle: nil)
//            let control = storyBoard.instantiateViewController(withIdentifier: "NewPasswordScreen") as! NewPasswordScreen
//            control.phone = self.mobileNumber
//            control.countryCode = self.countryCode
//            self.navigationController?.pushViewController(control, animated: true)
//        }
    }
    
    func goLoginScreen(){
        Utility.removeLocalData()
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginOptionScreen") as! LoginOptionScreen
        let rootvc = UINavigationController(rootViewController: control)
        appDelegate.window?.rootViewController = rootvc
        appDelegate.window?.makeKeyAndVisible()
    }
    
    //MARK:- Check OTP
    func checkOTP(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let verificationCode = "\(self.otpTextField1.text!)\(self.otpTextField2.text!)\(self.otpTextField3.text!)\(self.otpTextField4.text!)\(self.otpTextField5.text!)\(self.otpTextField6.text!)"
            let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID ?? "",
                verificationCode: verificationCode)
            
            FirebaseAuth.Auth.auth().signIn(with: credential) { (authResult, error) in
                
                if let error = error {
                    let authError = error as NSError
                    if ( authError.code == AuthErrorCode.secondFactorRequired.rawValue){
                        
                        // The user is a multi-factor user. Second factor challenge is required.
                        let resolver = authError.userInfo[AuthErrorUserInfoMultiFactorResolverKey] as! MultiFactorResolver
                        var displayNameString = ""
                        for tmpFactorInfo in (resolver.hints) {
                            displayNameString += tmpFactorInfo.displayName ?? ""
                            displayNameString += " "
                        }
                        
                    } else {
                        Utility.hideIndicator()
                        Utility.showAlert(vc: self, message: error.localizedDescription)
                        return
                    }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: self, message: error.localizedDescription)
                    return
                }else{
                    
                    let currentUser = FirebaseAuth.Auth.auth().currentUser
                    currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                        if let error = error {
                            Utility.hideIndicator()
                            Utility.showAlert(vc: self, message: error.localizedDescription)
                            return
                        }
                        print("Success")
                        //                        print("idToken =========>\(idToken)")
                        //                        self.loginAPI(idToken: idToken ?? "", phoneNumber: self.mobileNumber ?? "")
                        Utility.hideIndicator()
                        self.goFurther()
                    }
                }
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK: - Logout API
    func logOutApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.logout { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //                if let res = response{
                self?.goLoginScreen()
                //                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- TEXTFIELDS  DELEGATES
extension OtpScreen: UITextFieldDelegate{
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case self.otpTextField1:
                self.otpTextField2.becomeFirstResponder()
            case self.otpTextField2:
                self.otpTextField3.becomeFirstResponder()
            case self.otpTextField3:
                self.otpTextField4.becomeFirstResponder()
            case self.otpTextField4:
                self.otpTextField5.becomeFirstResponder()
            case self.otpTextField5:
                self.otpTextField6.becomeFirstResponder()
            case self.otpTextField6:
//                self.setLoginBtn()
                return
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case self.otpTextField1:
                self.otpTextField1.becomeFirstResponder()
            case self.otpTextField2:
                self.otpTextField1.becomeFirstResponder()
            case self.otpTextField3:
                self.otpTextField2.becomeFirstResponder()
            case self.otpTextField4:
                self.otpTextField3.becomeFirstResponder()
            case self.otpTextField5:
                self.otpTextField4.becomeFirstResponder()
            case self.otpTextField6:
                self.otpTextField5.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
   
}
