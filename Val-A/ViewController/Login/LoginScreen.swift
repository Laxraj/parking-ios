//
//  LoginScreen.swift
//  Val-A
//
//  Created by iroid on 08/02/21.
//

import UIKit
import GCCountryPicker

class LoginScreen: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", "US"))
        self.setPrivacyPolicyLabel()
        self.setupCallingCode()
        // Do any additional setup after loading the view.
    }
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            codeLabel.text = code
            countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", countryCode))
        }
    }
    @IBAction func onCreateAccount(_ sender: UIButton) {        
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "WhoAreYouScreen") as! WhoAreYouScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordScreen") as! ResetPasswordScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onCode(_ sender: Any) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func setPrivacyPolicyLabel(){
        let string  = "By continuing you are agreeing to Val-A’s, Terms of Use, and confirm that you have read and agree with Val-A’s, Privacy Policy."
        let attributedString = NSMutableAttributedString(string: string)
        let termsRange = (string as NSString).range(of: "Terms of Use")
        let privacyPolicyRange = (string as NSString).range(of: "Privacy Policy.")
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 15)!,NSAttributedString.Key.underlineColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: termsRange)
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "SourceSansPro-Bold", size: 15)!,NSAttributedString.Key.underlineColor : UIColor.black,NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue], range: privacyPolicyRange)
        
        self.privacyLabel.attributedText = attributedString
        self.privacyLabel.isUserInteractionEnabled = true
        self.privacyLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = privacyLabel.attributedText?.string else {
            return
        }

        if let range = text.range(of: NSLocalizedString("Terms of Use", comment: "terms")),
           recognizer.didTapAttributedTextInLabel(label: self.privacyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: teamOfUseLink)
        } else if let range = text.range(of: NSLocalizedString("Privacy Policy.", comment: "privacy")),
            recognizer.didTapAttributedTextInLabel(label: self.privacyLabel, inRange: NSRange(range, in: text)) {
            self.openUrl(strUrl: privacyLink)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func openUrl(strUrl:String){
        if let url = URL(string: strUrl) {
            UIApplication.shared.open(url)
        }
    }
    
    func checkValidation() -> String?{
        if self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone number"
        }else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter password"
        }else if passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count < 8{
            return "Please enter valide password"
        }
        return nil
    }
    
    @IBAction func onLogin(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.loginAPI()
        }
    }
    
    func goFurther(){
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    func callRegisterForPushAPI(){
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.registerForPush()
    }
    //MARK:- Login API
    func loginAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let phoneNumber = (self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
            let data = LoginRequest(phone: phoneNumber, password: self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), country_code: (self.codeLabel.text ?? ""))
            LoginService.shared.login(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                    
                    let userData = res
                    userData.isOTPVerify = true
                    Utility.saveUserData(data: userData.toJSON() )
                    Utility.setTabRoot()
                    
//                    Utility.saveUserData(data: res.toJSON())
//                    self?.callRegisterForPushAPI()
//                    let storyBoard = UIStoryboard(name: "Login", bundle: nil)
//                    let control = storyBoard.instantiateViewController(withIdentifier: "OtpScreen") as! OtpScreen
//                    control.screenType = OtpScreenNavigationType.afterLogin.rawValue
//                    control.countryCode = res.country_code //(self.codeLabel.text ?? "")
//                    control.mobileNumber = res.phone //(self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
//                    self?.navigationController?.pushViewController(control, animated: true)
//                    Utility.setTabRoot()
//                    Utility.showAlert(vc: self!, message: "login successfully done")
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- GCCountryPicker Delegate & DataSource
extension LoginScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
         self.dismiss(animated: true, completion: nil)
     }
     
     func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
         self.codeLabel.text = country.callingCode
         countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", country.countryCode))
         self.dismiss(animated: true, completion: nil)
     }
}
