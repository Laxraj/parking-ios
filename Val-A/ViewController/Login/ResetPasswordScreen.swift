//
//  ResetPasswordScreen.swift
//  Val-A
//
//  Created by iroid on 08/02/21.
//

import UIKit
import GCCountryPicker

class ResetPasswordScreen: UIViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", "US"))
        self.setupCallingCode()
        // Do any additional setup after loading the view.
    }
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            codeLabel.text = code
            countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", countryCode))
        }
    }
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onReset(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.goFurther()
        }
    }
    @IBAction func onCode(_ sender: Any) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone"
        }
        return nil
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func goFurther(){
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "OtpScreen") as! OtpScreen
        control.screenType = OtpScreenNavigationType.forgotPassword.rawValue
        control.mobileNumber = (self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        control.countryCode = (self.codeLabel.text ?? "")
        self.navigationController?.pushViewController(control, animated: true)
    }
    
}
//MARK:- GCCountryPicker Delegate & DataSource
extension ResetPasswordScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
         self.dismiss(animated: true, completion: nil)
     }
     
     func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
         self.codeLabel.text = country.callingCode
         countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", country.countryCode))
         self.dismiss(animated: true, completion: nil)
     }
}
