//
//  PayParkPopUpScreen.swift
//  Val-A
//
//  Created by Nikunj on 07/03/21.
//

import UIKit

class PayParkPopUpScreen: UIViewController {

    var onClose : ((Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onCard(_ sender: Any) {
        self.dismiss(animated: false) { [weak self] in
            self?.onClose?(false)
        }
    }
    
    @IBAction func onApplePay(_ sender: Any) {
        self.dismiss(animated: false) { [weak self] in
            self?.onClose?(true)
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    /*
     @IBAction func onApplePAy(_ sender: Any) {
     }
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
