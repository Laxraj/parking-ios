//
//  confirmTimeExtensionScreen.swift
//  Val-A
//
//  Created by iroid on 04/03/21.
//

import UIKit

class confirmTimeExtensionScreen: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!

//    var onClose: (() -> Void)?
    var onConfirm: (() -> Void)?

    var message = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        initialDetail()
    }
    func initialDetail(){
        messageLabel.text = message
    }
  
    @IBAction func onCancel(_ sender: UIButton) {
        self.dismiss(animated: false, completion: { [weak self] in
//            self?.onClose?()
        })
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        self.dismiss(animated: false, completion: { [weak self] in
            self?.onConfirm?()
        })
    }
    
}
