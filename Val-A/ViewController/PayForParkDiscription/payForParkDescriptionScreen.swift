//
//  payForParkDescriptionScreen.swift
//  Val-A
//
//  Created by iroid on 03/03/21.
//

import UIKit
import PassKit
class payForParkDescriptionScreen: UIViewController {

    @IBOutlet weak var howMunchLongerView: UIView!
    @IBOutlet weak var howMunchLongerViewConstant: NSLayoutConstraint!
    @IBOutlet weak var howMunchLongerViewBottomConstant: NSLayoutConstraint!
    @IBOutlet weak var sellMySpot: UIButton!
    @IBOutlet weak var zoneLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var zoneNameLabel: UILabel!
    @IBOutlet weak var zoneAddressLabel: UILabel!
    @IBOutlet weak var sellMySpotHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var sellMySpotTopConstant: NSLayoutConstraint!
    
    
    var obj: PayForParkResponse?
    
    var releaseDate: Date?
    var countdownTimer = Timer()
    
    var Min30Price: Double = 0.0
    var Min60Price: Double = 0.0
    
    var min = "30:00"
    var tabVC: TabbarScreen?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

        intialDetail()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.countdownTimer.invalidate()
        self.obj = nil
    }
    
    func intialDetail(){
        hideHowMuchLoger(isHide: true)
        self.setData()
        self.startTimer()
    }
   
    func setData(){
        if let data = self.obj{
            self.zoneLabel.text  = "Zone id: "+(data.zone_id ?? "")
            self.zoneNameLabel.text = "Zone name: "+(data.zone_name ?? "")
            self.zoneAddressLabel.text = "Zone address: "+(data.zone_address ?? "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getParkingTimeDurationAPI()
        if self.obj == nil{
            self.getTimer()
        }
    }
    
    func startTimer() {

        let releaseDateString = self.obj?.expiry_time ?? ""
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        releaseDate = releaseDateFormatter.date(from: releaseDateString)! as Date

        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }

    @objc func updateTime() {

        let currentDate = Date()
        let calendar = Calendar.current

        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: releaseDate! as Date)

        let countdown = "Days \(diffDateComponents.day ?? 0), Hours \(diffDateComponents.hour ?? 0), Minutes \(diffDateComponents.minute ?? 0), Seconds \(diffDateComponents.second ?? 0)"
        
        self.timerLabel.text = String(format: "%02d:%02d:%02d", diffDateComponents.hour ?? 0, diffDateComponents.minute ?? 0, diffDateComponents.second ?? 0)

        print(countdown)
        if diffDateComponents.hour == 0 && diffDateComponents.minute == 0 && diffDateComponents.second == 0{
            self.countdownTimer.invalidate()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func onCancel(_ sender: UIButton) {
        hideHowMuchLoger(isHide: true)
    }
    
    @IBAction func on60Min(_ sender: UIButton) {
       
        let vc = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "confirmTimeExtensionScreen") as! confirmTimeExtensionScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "Are you sure that you want to extend your time by 60 minutes?"
        vc.onConfirm = { [weak self] in
//            self?.extendTimeAPI(time: "60:00")
            let vc = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "PayParkPopUpScreen") as! PayParkPopUpScreen
            vc.modalPresentationStyle = .overCurrentContext
            vc.onClose = { (data) in
                self?.min = "60:00"
                if  !data{
                self?.hideHowMuchLoger(isHide: true)
                self?.extendTimeAPI(time: "60:00",isApple: data,price: String(describing: self?.Min60Price ?? 1), transactionId: "")
                }else{
                    self?.onApplePayment(price: Int(self?.Min60Price ?? 1.0))
                }
            }
            self?.present(vc, animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func on30Min(_ sender: UIButton) {
        let vc = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "confirmTimeExtensionScreen") as! confirmTimeExtensionScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "Are you sure that you want to extend your time by 30 minutes?"
        vc.onConfirm = { [weak self] in
            let vc = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "PayParkPopUpScreen") as! PayParkPopUpScreen
            vc.modalPresentationStyle = .overCurrentContext
            vc.onClose = { (data)  in
                self?.min = "30:00"
                if  !data{
                self?.hideHowMuchLoger(isHide: true)
                    self?.extendTimeAPI(time: "30:00",isApple: data,price: String(describing: self?.Min30Price ?? 1), transactionId: "")
                }else{
                    self?.onApplePayment(price: Int(self?.Min30Price ?? 1.0))
                }
            }
            self?.present(vc, animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onExtendTime(_ sender: UIButton) {
        hideHowMuchLoger(isHide: false)
    }
    
    @IBAction func onSellMySplot(_ sender: UIButton) {
        self.getAuctionTimer()
    }
    
    func getAuctionTimer(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuctionServices.shared.getAuctionTimer(success: { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                guard let stronSelf = self else { return }
                print(response?.toJSON())
                if let res = response{
                    if res.is_timmer == 0 && res.exchange_sports == false && res.place_bid == false{
                        self?.goAuctionScreen()
                    }else{
                        Utility.showAlert(vc: stronSelf, message: "Your already other auction is ongoing.")
                    }
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
                self?.goAuctionScreen()
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func goAuctionScreen(){
//
//        self.tabVC?.setUpTabBar()
//        self.tabVC?.zoneID = "0"
//        self.tabVC?.selectedIndex = 2
//        self.tabVC?.viewControllers?[2]
        //        self.tabVC?.zoneID = self.obj?.zone_id ?? "0"

        
        Utility.setSellMySpotTabRoot(zoneId: self.obj?.zone_id ?? "0")
    }
    
    @IBAction func endParking(_ sender: UIButton) {
        self.endParkingTimeDurationAPI()
    }
    func hideHowMuchLoger(isHide:Bool){
        if isHide{
            setView(view: howMunchLongerView, hidden: true)
//            sellMySpot.isHidden = false
//            howMunchLongerViewConstant.constant = 0
//            sellMySpotHeightConstant.constant = 40
//            sellMySpotTopConstant.constant = 12
//            howMunchLongerViewBottomConstant.constant = 0
            
            sellMySpot.isHidden = true
            sellMySpotHeightConstant.constant = 0
            sellMySpotTopConstant.constant = 0
            howMunchLongerViewConstant.constant = 80
            howMunchLongerViewBottomConstant.constant = 12
        }else{
            setView(view: howMunchLongerView, hidden: false)
            sellMySpot.isHidden = true
            sellMySpotHeightConstant.constant = 0
            sellMySpotTopConstant.constant = 0
            howMunchLongerViewConstant.constant = 80
            howMunchLongerViewBottomConstant.constant = 12
        }
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    func onApplePayment(price:Int){
        let paymentNetwork = [ PKPaymentNetwork.amex,.discover, .masterCard,.visa]
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetwork){
            let request = PKPaymentRequest()
            request.merchantIdentifier = "merchant.com.vala.mobile"
            request.supportedNetworks = paymentNetwork
            request.supportedCountries = [ "US"]
            //            request.merchantCapabilities = .capabilityEMV
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.countryCode = "US"
            request.currencyCode = "USD"
            request.paymentSummaryItems = [PKPaymentSummaryItem(label: "parking zone", amount: NSDecimalNumber(value: price))]
            let applePayvc = PKPaymentAuthorizationViewController(paymentRequest: request)
            applePayvc?.delegate = self
            self.present(applePayvc!, animated: true, completion: nil)
        }
    }
    
    func extendTimeAPI(time: String,isApple :Bool,price: String,transactionId:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ExtendTimeRequest(extended_time: time, zone_id: self.obj?.zone_id, transaction_type: "1", payment_type: isApple ? "2" : "1", price: price, payment_status: "success", transaction_id: transactionId)
            PayParkService.shared.extendMoreTime(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let data = response.payForParkResponse{
                    self?.obj = data
                    self?.refreshTimer()
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- GET TIMER
    func getTimer(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            
            PayParkService.shared.getTimer { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let data = response.payForParkResponse{
                    if data.is_timmer == 1{
                        self?.obj = data
                        self?.refreshTimer()
                    }else{
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    //MARK:- GET PRICE
    func getParkingTimeDurationAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            PayParkService.shared.getParkingTimeDurationList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    if let min30 = res.first(where: {$0.durations == "30 min"}){
                        self.Min30Price = min30.price ?? 0.0
                    }
                    if let min60 = res.first(where: {$0.durations == "01 hour"}){
                        self.Min60Price = min60.price ?? 0.0
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    //MARK:- EndTimer
    func endParkingTimeDurationAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            PayParkService.shared.endTimer{ (statusCode, response) in
                Utility.hideIndicator()
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: PayForParkScreen.self) {
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
//                self.navigationController?.popViewController(animated: true)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func refreshTimer(){
        self.countdownTimer.invalidate()
        self.setData()
        self.startTimer()
    }
    
  
}
extension payForParkDescriptionScreen : PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
//        payment.token
//        payment.billingContact?.phoneNumber
        
              let paymentToken = payment.token
              let billingContact = payment.billingContact
              let shippingContact = payment.shippingContact
              let shippingMethod = payment.shippingMethod
              
              print("Religion: \(String(describing: billingContact))")
              print("Sending information: \(String(describing: shippingContact))")
              print("Delivery method: \(String(describing: shippingMethod))")
              print("Successful trade! ......\(paymentToken)")
               print(payment.token.transactionIdentifier)
        
        self.extendTimeAPI(time: min,isApple: true,price: String(describing: min == "30:00" ? self.Min30Price :self.Min60Price ), transactionId:payment.token.transactionIdentifier)
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: nil))
    }
}
