//
//  AddCardScreen.swift
//  Val-A
//
//  Created by Nikunj on 21/02/21.
//

import UIKit

class AddCardScreen: UIViewController {

    @IBOutlet weak var expirationTextField: dateSportTextField!
    @IBOutlet weak var cardNumberTextField: dateSportTextField!
    @IBOutlet weak var cvvTextField: dateSportTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDatePicker()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    
    func setData(){
        if let res = Utility.getUserInfoData(){
            self.expirationTextField.text = res.expiration_date
            self.cardNumberTextField.text = res.card_number
            self.cvvTextField.text = res.cvv
        }
    }

    func setDatePicker(){
        let expiryDatePicker = MonthYearPickerView()
        self.expirationTextField.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year % 100)
            NSLog(string) // should show something like 05/2015
            self.expirationTextField.text = string
        }
    }
    
    @IBAction func onSaveCard(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.updateUserData()
        }
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateUserData(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = UpdateUserInfoRequest(first_name: Utility.getUserInfoData()?.first_name, last_name: Utility.getUserInfoData()?.last_name, phone: Utility.getUserInfoData()?.phone, password: nil, vehicle_make: nil, vehicle_model: nil, vehicle_year: nil, vehicle_color: nil, license_plate_no: Utility.getUserInfoData()?.license_plate_no, card_number: self.cardNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), expiration_date: self.expirationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), cvv: self.cvvTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), country_code: nil)
            LoginService.shared.updateUserInfo(parameters: data.toJSON(), image: nil, success: { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                    Utility.setUserInfoData(data: res.toJSON())
                    self.openSucess()
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func openSucess(){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "You have successfully added your new card. You will be redirected back to your parking summary page."
        vc.onClose = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.cardNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 < 14{
            return "Please enter proper card number"
        }else if self.expirationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter expiration date"
        }else if self.cvvTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter cvv"
        }
        return nil
    }
}
