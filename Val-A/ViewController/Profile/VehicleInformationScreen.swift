//
//  VehicleInformationScreen.swift
//  Val-A
//
//  Created by Nikunj on 21/02/21.
//

import UIKit
import DropDown

class VehicleInformationScreen: UIViewController {
    @IBOutlet weak var vehicleYearTextField: dateSportTextField!
    @IBOutlet weak var vehicleMakeTextField: dateSportTextField!
    @IBOutlet weak var vehicleModelTextField: dateSportTextField!
    @IBOutlet weak var licensePlateTextField: dateSportTextField!
    @IBOutlet weak var vehicleColorTextField: dateSportTextField!
    
    var vehicleMakeArray: [VehicleMakeResponse] = []
    var vehicleModelArray: [VehicleModelResponse] = []
    var vehicleYearArray: [VehicleYearResponse] = []
    var vehicleColorArray: [VehicleColorResponse] = []
    
    var vehicleYearId = 0
    var makeId = 0
    var modelId = 0
    var colorId  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.getVehicleListAPI()
        self.getVehicleYearListAPI()
        self.getVehicleColorListAPI()
        // Do any additional setup after loading the view.
    }
    
    func setData(){
        if let res = Utility.getUserInfoData(){
            self.vehicleYearTextField.text = res.vehicle_year_name
            self.vehicleMakeTextField.text = res.vehicle_make_name
            self.vehicleModelTextField.text = res.vehicle_model_name
            self.vehicleColorTextField.text = res.vehicle_color_name
            self.licensePlateTextField.text = res.license_plate_no
            
            if let data = self.vehicleMakeArray.first(where: {$0.vehicle_make_id == res.vehicle_make ?? 0}){
                self.vehicleModelArray = data.models ?? []
            }
        }
    }
    
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onVehicleModel(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleModelArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 1, view: self.vehicleModelTextField)
    }
    @IBAction func onVehicleMake(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleMakeArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 0, view: self.vehicleMakeTextField)
    }
    @IBAction func onVehicleYear(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleYearArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 2, view: self.vehicleYearTextField)
    }
    @IBAction func onVehicleColor(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleColorArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 3, view: self.vehicleColorTextField)
    }
    
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
                self?.vehicleMakeTextField.text = item
                self?.vehicleModelTextField.text = nil
                self?.vehicleModelArray = self?.vehicleMakeArray[index].models ?? []
                dropDown.hide()
            case 1:
                self?.vehicleModelTextField.text = item
                dropDown.hide()
            case 2:
                self?.vehicleYearTextField.text = item
                dropDown.hide()
            default:
                self?.vehicleColorTextField.text = item
                dropDown.hide()
            }
        }
    }
    
    @IBAction func onSave(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            var vehicleMakeID: String?
            var vehicleModelID: String?
            var vehicleYearID: String?
            var vehicleColorID: String?
            
            if let val = self.vehicleMakeArray.first(where: {$0.name == self.vehicleMakeTextField.text}){
                vehicleMakeID = "\(val.vehicle_make_id ?? 0)"
            }
            if let val = self.vehicleModelArray.first(where: {$0.name == self.vehicleModelTextField.text}){
                vehicleModelID = "\(val.id ?? 0)"
            }
            if let val = self.vehicleYearArray.first(where: {$0.name == self.vehicleYearTextField.text}){
                vehicleYearID = "\(val.vehicle_year_id ?? 0)"
            }
            
            if let val = self.vehicleColorArray.first(where: {$0.name == self.vehicleColorTextField.text}){
                vehicleColorID = "\(val.vehicle_color_id ?? 0)"
            }
            
            self.updateUserData(vehicleMakeId: vehicleMakeID ?? "0", vehicleModelId: vehicleModelID ?? "0", vehicleYear: vehicleYearID ?? "0", vehicleColor: vehicleColorID ?? "0")
        }
    }
    
    //MARK:- UPDATE USER DATA
    func updateUserData(vehicleMakeId:String,vehicleModelId:String,vehicleYear:String,vehicleColor:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = UpdateUserInfoRequest(first_name:nil, last_name: nil, phone: nil, password: nil, vehicle_make: vehicleMakeId, vehicle_model: vehicleModelId, vehicle_year: vehicleYear, vehicle_color: vehicleColor, license_plate_no: self.licensePlateTextField.text, card_number: nil, expiration_date: nil, cvv: nil, country_code: nil)
            
            print(data.toJSON())
            LoginService.shared.updateUserInfo(parameters: data.toJSON(), image: nil, success: { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                    Utility.setUserInfoData(data: res.toJSON())
                    self.openSucess()
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func openSucess(){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "You have successfully added your new vehicle."
        vc.onClose = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:- CHECK VALIDATION
    func checkValidation() -> String?{
        
        if self.vehicleYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle year"
        }else if self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle make"
        }else if self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle model"
        }else if self.vehicleColorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please choose vehicle color"
        }else if self.licensePlateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter plate number"
        }
        return nil
    }
    
    //MARK:- Vehicle List API
    func getVehicleListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleList { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleMakeArray = res
                    self.setData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Year API
    func getVehicleYearListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleYearList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleYearArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Color API
    func getVehicleColorListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.getVehicleColorList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.vehicleColorArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
