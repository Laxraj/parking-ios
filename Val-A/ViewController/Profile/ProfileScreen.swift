//
//  ProfileScreen.swift
//  Val-A
//
//  Created by iroid on 20/02/21.
//

import UIKit

class ProfileScreen: UIViewController {
    
    @IBOutlet weak var accountInformationView: UIView!
    @IBOutlet weak var accountInformationViewConstant: NSLayoutConstraint!
    @IBOutlet weak var accountInformationInnerView: UIView!
    
    @IBOutlet weak var vehicleInformationView: UIView!
    @IBOutlet weak var vehicleAccountInformationViewConstant: NSLayoutConstraint!
    @IBOutlet weak var vehicleAccountInformationInnerView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var paymentInformationView: UIView!
    @IBOutlet weak var paymentInformationViewConstant: NSLayoutConstraint!
    @IBOutlet weak var paymentHistoryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paymenttInformationInnerView: UIView!
    @IBOutlet weak var historyUnderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var historyView: dateSportView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardTypeLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var vehicleLabel: UILabel!
    @IBOutlet weak var vehicleModel: UILabel!
    @IBOutlet weak var vehicleColor: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var cashOutPayment: UILabel!
    @IBOutlet weak var transactionHistoryTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var accountInformationDropDownImageView: UIImageView!
    @IBOutlet weak var vahicleInformationDropDownImageView: UIImageView!
    @IBOutlet weak var paymentInformationDropDownImageView: UIImageView!
    @IBOutlet weak var transactionInformationDropDownImageView: UIImageView!
    @IBOutlet weak var addBankButton: UIButton!
    @IBOutlet weak var pandingLabel: UILabel!
    
    var itemArray: [TransactionHistoryResponse] = []
    var stripStatus = 0
    var stripData:StripStatusResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.historyTableView.delegate = self
        self.historyTableView.dataSource = self
        self.historyTableView.register(UINib(nibName: "TransactionDetailCell", bundle: nil), forCellReuseIdentifier: "TransactionDetailCell")
        initialDetail()
        self.getUserDataApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         //getUserDataApi()
        self.setUserData()
        self.getTransaction()
        self.getAccountSetUpAPI()
    }
    
    func setUserData(){
        if let res = Utility.getUserInfoData(){
            self.nameLabel.text = "Name: \(res.first_name ?? "") \(res.last_name ?? "")"
            self.emailLabel.text = "Phone: \(res.country_code ?? "") \(res.phone ?? "")"
            self.passwordLabel.text = "Password: **********"
            self.cardTypeLabel.text = "Card Type : \(res.card_brand ?? "")"
            self.cardNumberLabel.text = "Card Number: ****\(res.card_number?.suffix(4) ?? "")"
            self.expirationDateLabel.text = "Expiration Date: \(res.expiration_date ?? "")"
            self.cvvLabel.text = "CVV: \(res.cvv ?? "")"
            self.yearLabel.text = "Year: \(res.vehicle_year_name ?? "")"
            self.vehicleLabel.text = "Make: \(res.vehicle_make_name ?? "")"
            self.vehicleModel.text = "Model: \(res.vehicle_model_name ?? "")"
            self.vehicleColor.text = "Color: \(res.vehicle_color_name ?? "")"
            self.plateLabel.text = "Plate #: \(res.license_plate_no ?? "")"
//            self.cashOutPayment.text = "$ " + String(format:"%.02f", res.availableBalance ?? 0.0)
            Utility.setImage(res.profile_pic, imageView: profileImageView)
        }
    }
    
    
    
    func initialDetail(){
        accountInformationViewConstant.constant = 34
        accountInformationInnerView.isHidden = true
        
        vehicleAccountInformationViewConstant.constant = 20
        vehicleAccountInformationInnerView.isHidden = true
        
        paymentInformationViewConstant.constant = 26
        paymenttInformationInnerView.isHidden = true
        
        self.paymentHistoryViewHeight.constant = 56
        self.paymentHistoryViewHeight.priority = UILayoutPriority(1000)
        self.historyUnderViewHeight.constant = 0
        self.historyView.isHidden = true
    }
    
    @IBAction func onEditAccountInformation(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "AccountInformationScreen") as! AccountInformationScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onEditVehicleInformation(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "VehicleInformationScreen") as! VehicleInformationScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onAddCard(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "AddCardScreen") as! AddCardScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    
    @IBAction func onLogout(_ sender: Any) {
        //        logOutApi()
        showLogOutOptions()
    }
    
    @IBAction func onAccountInformation(_ sender: UIButton) {
        self.setView(view: accountInformationInnerView, hidden: !self.accountInformationInnerView.isHidden)
        if sender.isSelected{
            sender.isSelected = false
            accountInformationViewConstant.constant = 34
            accountInformationInnerView.isHidden = true
            accountInformationDropDownImageView.image = UIImage(named: "drop_up_arrow_icon")
        }else{
            sender.isSelected = true
            accountInformationViewConstant.constant = 140
            accountInformationInnerView.isHidden = false
            accountInformationDropDownImageView.image = UIImage(named: "drop_down_arrow_icon")
        }
    }
    
    @IBAction func onVehicleInformation(_ sender: UIButton) {
        self.setView(view: vehicleAccountInformationInnerView, hidden: !self.vehicleAccountInformationInnerView.isHidden)
        if sender.isSelected{
            sender.isSelected = false
            vehicleAccountInformationViewConstant.constant = 20
            vehicleAccountInformationInnerView.isHidden = true
            vahicleInformationDropDownImageView.image = UIImage(named: "drop_up_arrow_icon")
        }else{
            sender.isSelected = true
            vehicleAccountInformationViewConstant.constant = 150
            vehicleAccountInformationInnerView.isHidden = false
            vahicleInformationDropDownImageView.image = UIImage(named: "drop_down_arrow_icon")
        }
    }
    
    @IBAction func onPaymentInformation(_ sender: UIButton) {
        self.setView(view: paymenttInformationInnerView, hidden: !self.paymenttInformationInnerView.isHidden)
        if sender.isSelected{
            sender.isSelected = false
            paymentInformationViewConstant.constant = 26
            paymenttInformationInnerView.isHidden = true
            paymentInformationDropDownImageView.image = UIImage(named: "drop_up_arrow_icon")
        }else{
            sender.isSelected = true
            paymentInformationViewConstant.constant = 160
            paymenttInformationInnerView.isHidden = false
            paymentInformationDropDownImageView.image = UIImage(named: "drop_down_arrow_icon")
        }
    }
    
    @IBAction func onTransactionHistory(_ sender: UIButton) {
        self.setView(view: historyView, hidden: !self.historyView.isHidden)
        if sender.isSelected{
            sender.isSelected = false
            self.paymentHistoryViewHeight.constant = 56
            self.paymentHistoryViewHeight.priority = UILayoutPriority(1000)
            self.historyUnderViewHeight.constant = 0
            self.historyView.isHidden = true
            self.transactionInformationDropDownImageView.image = UIImage(named: "drop_up_arrow_icon")
        }else{
            sender.isSelected = true
            self.paymentHistoryViewHeight.constant = 206
            self.paymentHistoryViewHeight.priority = UILayoutPriority(250)
            self.historyUnderViewHeight.constant = 150
            self.historyView.isHidden = false
            self.transactionInformationDropDownImageView.image = UIImage(named: "drop_down_arrow_icon")
        }
    }
    
    @IBAction func onUpload(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
        
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func onCashOut(_ sender: UIButton) {
        if let res = Utility.getUserInfoData(){
            if res.stripe_status == 0 || res.stripe_status == 2{
                self.showSimpleAlert()
            }else{
                if  stripData?.availableBalance != 0.0{
                    self.getPaymentAlert()
                }else{
                    Utility.showAlert(vc: self, message: "Your balance is 0.0 so can't withdraw amount")
                }
            }
        }
    }
    
    func getPaymentAlert() {
        let alert = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to Withdraw your Balance?",         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.getWithdrawAmountApi()
//            self.getPayOut(amount: "\(self.availablePayOutBalance)")
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSimpleAlert() {
        let alert = UIAlertController(title: APP_NAME, message: "need to add account for cash out payment",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
              //Cancel Action
          }))
          alert.addAction(UIAlertAction(title: "Ok",
                                        style: UIAlertAction.Style.default,
                                        handler: {(_: UIAlertAction!) in
                                          //Sign out action
                                            self.createStripAccount()
          }))
          self.present(alert, animated: true, completion: nil)
      }
    
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    func showLogOutOptions() {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        self.logOutApi()
                                      }))
        self.present(alert, animated: true, completion: nil)
    }
    func goLoginScreen(){
        Utility.removeLocalData()
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginOptionScreen") as! LoginOptionScreen
        let rootvc = UINavigationController(rootViewController: control)
        appDelegate.window?.rootViewController = rootvc
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    func updateUserData(imageData:Data){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = UpdateUserInfoRequest(first_name:nil, last_name: nil, phone: nil, password: nil, vehicle_make: nil, vehicle_model: nil, vehicle_year: nil, vehicle_color: nil, license_plate_no: nil, card_number: nil, expiration_date: nil, cvv: nil, country_code: nil)
            LoginService.shared.updateUserInfo(parameters: data.toJSON(), image: imageData, success: { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                    Utility.setUserInfoData(data: res.toJSON())
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    func createStripAccount(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.createStripAccount(parameters: [:]) { [weak self] (statusCode, response) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                if let res = response.stripAccountCreateResponse{
                    print(res.toJSON())
                    let control = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
                    control.linkUrl = res.url ?? "" 
//                    control.onBack = {[weak self ] in
//                        self?.getAccountSetUpAPI()
//                    }
                    self?.navigationController?.pushViewController(control, animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: "User login Successfully")
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getUserDataApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           //Utility.showIndicator()
            LoginService.shared.getUserInfo { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    Utility.setUserInfoData(data: res.toJSON())
                    self?.setUserData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    func getWithdrawAmountApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           Utility.showIndicator()
            let data = WithdrawPaymentRequest(amount: String(format:"%.02f", stripData?.availableBalance ?? 0.0))
            LoginService.shared.WithdrawAmount(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                
                self?.showSuccessAlert(message: response?.message ?? "")
                
//                if let res = response{
//                    Utility.setUserInfoData(data: res.toJSON())
//                    self?.setUserData()
//                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func showSuccessAlert(message:String) {
        let alert = UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            self.getAccountSetUpAPI()
//            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getAccountSetUpAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndicator()
            LoginService.shared.getAccountStatus(parameters: [:]) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.stripStatusResponse{
                    self?.stripData = res
                    self?.cashOutPayment.text = "$ " + String(format:"%.02f", res.availableBalance ?? 0.0)
                    self?.stripStatus = res.stripe_status ?? 0
                    if self?.stripStatus == 2{
                        self?.pandingLabel.isHidden = false
                        //for temp
//                        self?.pandingLable.isHidden = true
                        self?.pandingLabel.text = "Your account is pending"
                        self?.addBankButton.setTitle("Add Bank", for: .normal)
//                        self?.addBankButton.isHidden = false
                    }else if self?.stripStatus == 1{
                        self?.pandingLabel.isHidden = false
                        
                        //for temp
                        self?.pandingLabel.isHidden = false
                        self?.pandingLabel.textColor = #colorLiteral(red: 0, green: 0.8509803922, blue: 0.03137254902, alpha: 1)
                        self?.pandingLabel.text = "Funds under processing \(res.pendingBalance ?? 0.0)"
                        self?.addBankButton.setTitle("Withdraw", for: .normal)
//                        self?.addBankButton.isHidden = true
                    }else{
                        self?.pandingLabel.isHidden = true
                    }
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func logOutApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.logout { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //                if let res = response{
                self?.goLoginScreen()
                //                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getTransaction(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            PayParkService.shared.getTransaction {[weak self] (statusCode, response)  in
                Utility.hideIndicator()
                if let data = response{
                    self?.itemArray = data
                    self?.historyTableView.reloadData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
}
//MARK:- TABLEVIEW DELEGATE & DATASOURCE
extension ProfileScreen: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.historyTableView.dequeueReusableCell(withIdentifier: "TransactionDetailCell", for: indexPath) as! TransactionDetailCell
        cell.item = self.itemArray[indexPath.row]
        return cell
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
}
//MARK:- SET IMAGEVIEW
extension ProfileScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        profileImageView.image = pickedImage
        self.dismiss(animated: true, completion: { () -> Void in
            Utility.showIndicator()
            self.updateUserData(imageData: (pickedImage.resized()?.pngData())!)
        })
    }
    
    func openCamera()
    {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
}
