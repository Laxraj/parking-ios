//
//  SuccessScreen.swift
//  Val-A
//
//  Created by Nikunj on 21/02/21.
//

import UIKit

class SuccessScreen: UIViewController {
    @IBOutlet weak var messageLabel: UILabel!

    var onClose: (() -> Void)?
    var message = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        initialDetail()
    }
    
    func initialDetail(){
        messageLabel.text = message
    }
    @IBAction func onDone(_ sender: Any) {
        self.dismiss(animated: false, completion: { [weak self] in
            self?.onClose?()
        })
    }
    
}
