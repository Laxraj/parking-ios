//
//  ChangePasswordScreen.swift
//  Val-A
//
//  Created by iMac on 31/12/22.
//

import UIKit

class ChangePasswordScreen: UIViewController {

    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSave(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.changePassword()
        }
    }
    
    func checkValidation() -> String?{
        if self.currentPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter current password"
        }else if passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count < 8{
            return "Please enter valid new password"
        }else if self.confirmTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter re-enter password"
        }else if passwordTextField.text != confirmTextField.text{
            return "New password & re-enter password does not match"
        }
        return nil
    }
}
//MARK:- API
extension ChangePasswordScreen{
    
    func changePassword(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ChangePasswordRequest(current_password: self.currentPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),password: self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), passwordConfirmation: self.confirmTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            LoginService.shared.changePassword(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                self.openSuccess(message:response?.message ?? "")
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func openSuccess(message:String){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = message
        vc.onClose = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
}

