//
//  AccountInformationScreen.swift
//  Val-A
//
//  Created by iroid on 21/02/21.
//

import UIKit
import GCCountryPicker

class AccountInformationScreen: UIViewController {
    
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var verifyPasswordView: UIView!
    @IBOutlet weak var passworViewHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmPassworViewHeight: NSLayoutConstraint!
    @IBOutlet weak var codeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setData()
        // setupCallingCode()
    }
    
    func setData(){
        self.countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", "US"))
        if let res = Utility.getUserInfoData(){
            self.firstNameTextField.text = res.first_name
            self.lastNameTextField.text = res.last_name
            self.phoneTextField.text = res.phone
            self.codeLabel.text = res.country_code
            if let countryCode = res.country_code{
                self.countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", Utility.getCountryPhoneCode(String(countryCode.dropFirst()))))
            }
            if res.is_social_account ?? false{
                self.passwordView.isHidden = true
                self.verifyPasswordView.isHidden = true
                self.confirmPassworViewHeight.constant = 0
                self.passworViewHeight.constant = 0
            }
        }
    }
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            codeLabel.text = code
            countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", countryCode))
        }
    }
    
    
    @IBAction func onChange(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordScreen") as! ChangePasswordScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            if let res = Utility.getUserInfoData(){
                if self.phoneTextField.text == res.phone &&  self.codeLabel.text == res.country_code{
                    self.updateUserData()
                }else{
                    let storyBoard = UIStoryboard(name: "Login", bundle: nil)
                    let control = storyBoard.instantiateViewController(withIdentifier: "OtpScreen") as! OtpScreen
                    control.screenType = OtpScreenNavigationType.editProfile.rawValue
                    control.countryCode = self.codeLabel.text //(self.codeLabel.text ?? "")
                    control.mobileNumber = self.phoneTextField.text //(self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
                    control.onClose = { [weak self] in
                        self?.updateUserData()
                    }
                    self.navigationController?.pushViewController(control, animated: true)
                }
            }else{
                self.updateUserData()
            }
            
            
        }
    }
    
    @IBAction func onDelete(_ sender: UIButton) {
        self.deleteAccountAlert()
    }
    
    @IBAction func onCode(_ sender: Any) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func deleteAccountAlert() {
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to delete your account? All of your information and content will be deleted.", preferredStyle: UIAlertController.Style.alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (action) in
            self?.deleteUserApi()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateUserData(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = UpdateUserInfoRequest(first_name:firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), last_name: lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), phone: phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), password: passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), vehicle_make: nil, vehicle_model: nil, vehicle_year: nil, vehicle_color: nil, license_plate_no: nil, card_number: nil, expiration_date: nil, cvv: nil, country_code: self.codeLabel.text)
            LoginService.shared.updateUserInfo(parameters: data.toJSON(), image: nil, success: { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                    Utility.setUserInfoData(data: res.toJSON())
                    self.openSucess()
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func deleteUserApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            LoginService.shared.deleteAccount{ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                Utility.removeUserData()
                self?.goLoginScreen()
                //                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func goLoginScreen(){
        Utility.removeLocalData()
        let storyBoard = UIStoryboard(name: "Login", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "LoginOptionScreen") as! LoginOptionScreen
        let rootvc = UINavigationController(rootViewController: control)
        appDelegate.window?.rootViewController = rootvc
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func openSucess(){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "You have successfully update your profile information."
        vc.onClose = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter first name"
        }else if self.lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter last name"
        }else if self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone number"
        }else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            if passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count < 8{
                return "Please enter valide password"
            }else if self.confirmTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter verify password"
            }else if passwordTextField.text != confirmTextField.text{
                return "password are not match"
            }
        }
        return nil
    }
}

//MARK:- GCCountryPicker Delegate & DataSource
extension AccountInformationScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        self.codeLabel.text = country.callingCode
        countryImageView.image = UIImage(named: String(format: "CountryPicker.bundle/%@", country.countryCode))
        self.dismiss(animated: true, completion: nil)
    }
}
