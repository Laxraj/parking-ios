//
//  WebViewScreen.swift
//  Source-App
//
//  Created by iroid on 20/04/21.
//

import UIKit
import WebKit

class WebViewScreen: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    var webView: WKWebView!
    var linkUrl = ""
    
    var onBack : (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        //webView.navigationDelegate = self
        initialDetail()
        // Do any additional setup after loading the view.
    }
    
    func initialDetail(){
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.frame = CGRect(origin: CGPoint.zero, size: containerView.frame.size)
        
        let url = URL(string: linkUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        containerView.addSubview(webView!)
    }
    @IBAction func onBack(_ sender: UIButton) {
//        self.onBack!()
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension WebViewScreen: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
        Utility.showIndicator()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        Utility.hideIndicator()
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
