//
//  CreateAnAuctionDetailScreen.swift
//  Val-A
//
//  Created by iroid on 14/03/21.
//

import UIKit

class CreateAnAuctionDetailScreen: UIViewController {
    
    @IBOutlet weak var currentBidLabel: UILabel!
    @IBOutlet weak var startingBidLabel: UILabel!
    @IBOutlet weak var paidLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var zoneLabel: UILabel!
    
    @IBOutlet weak var zoneLocationTopConstant: NSLayoutConstraint!
    @IBOutlet weak var zoneLocatiBottomConstant: NSLayoutConstraint!

    var auctionObj: AuctionResponse?
    var releaseDate: Date?
    var countdownTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.tabBarController?.tabBar.isHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.countdownTimer.invalidate()
        self.auctionObj = nil
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.auctionObj == nil{
            self.getAuctionTimer()
        }
        
//        self.tabBarController?.tabBar.isHidden = true
//        self.tabBarController?.tabBar.layer.zPosition = -1
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.layer.zPosition = -0
    }
    
    //MARK:-  GET AUCTION TIMER
    func getAuctionTimer(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndicator()
            AuctionServices.shared.getAuctionTimer(success: { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                print(response?.toJSON())
                if let data = response{
                    if data.is_timmer == 1{
                        self?.auctionObj = data
                        self?.refreshTimer()
                    }else{
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }, failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func refreshTimer(){
        self.countdownTimer.invalidate()
        self.setData()
        self.startTimer()
    }
    
    func setData(){
        if let data = self.auctionObj{
//            self.paidLabel.text = data.free_paid == 2  ? "Paid: $\(data.fee ?? "0")/hour" : "Free: $\(data.fee ?? "0")/hour"
//
            self.paidLabel.text = data.free_paid == 2  ? "Paid: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour" : "Free: $\(String(format:"%.02f", Double(data.fee ?? "0") ?? 0.0))/hour"
            self.descriptionLabel.text = data.description
            if data.zone_name == ""{
                self.zoneLabel.text = ""
                self.zoneLocationTopConstant.constant = 0
            }else{
                self.zoneLabel.text = "Parking Location : \n"+(data.zone_name ?? "")
            }
            
            self.startingBidLabel.text = "Starting Bid: $"+"\(String(format:"%.02f", Double(data.startbid ?? "0") ?? 0.0))"
           
//            self.startingBidLabel.text = "Starting Bid: $"+(data.startbid ?? "0")
            self.currentBidLabel.text = "Current Bid: $"+"\(String(format:"%.02f", Double(data.highest_bid ?? "0") ?? 0.0))"
//            self.currentBidLabel.text = "Current Bid: $"+(data.highest_bid ?? "0")
            
            
            self.startTimer()
        }
    }
    
    func startTimer() {
        let releaseDateString = self.auctionObj?.time_of_exchange ?? ""
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        releaseDate = releaseDateFormatter.date(from: releaseDateString)! as Date
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.layer.zPosition = -0
        self.countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
        let currentDate = Date()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: releaseDate! as Date)
        
        let countdown = "Days \(diffDateComponents.day ?? 0), Hours \(diffDateComponents.hour ?? 0), Minutes \(diffDateComponents.minute ?? 0), Seconds \(diffDateComponents.second ?? 0)"
        
        self.timeLabel.text = String(format: "%02d:%02d:%02d", diffDateComponents.hour ?? 0, diffDateComponents.minute ?? 0, diffDateComponents.second ?? 0)
        
        print(countdown)
        
        let second =   Double(diffDateComponents.second ?? 0)
        if second.sign == .minus{
            self.countdownTimer.invalidate()
            self.navigationController?.popViewController(animated: true)
        }
//        if diffDateComponents.hour == 0 && diffDateComponents.minute == 0 && diffDateComponents.second == 0{
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                // your code here
//
//            }
//          }
    }
    
    @IBAction func onEndMyAuction(_ sender: Any) {
        let vc = STORYBOARD.auction.instantiateViewController(withIdentifier: "ConfirmAuctionScreen") as! ConfirmAuctionScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.auctionObj = self.auctionObj
        vc.onEndAuctions = { [weak self] in
            self?.navigationController?.popViewController(animated: false)
        }
        self.present(vc, animated: true, completion: nil)
    }
}
