//
//  ExchangeSpotsScreen.swift
//  Val-A
//
//  Created by iroid on 22/03/21.
//

import UIKit
import MapKit
class ExchangeSpotsScreen: UIViewController {

    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var vahicleImageView: dateSportImageView!
    
    @IBOutlet weak var waitingForPaymentLabel: UILabel!
    @IBOutlet weak var switchConfirmButton: UIButton!
    var auctionObj: AuctionResponse?
    var vahicaleDetail: AuctionVehicleDetailsResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(paymentDone(notfication:)), name: NSNotification.Name(rawValue: "PAYMENT_DONE"), object: nil)
        actionVehicleDetail()
    }
    
    func setData(){
        if let data = self.vahicaleDetail{
            Utility.setImage(data.profile_pic, imageView: vahicleImageView)
            self.yearLabel.text = "Year:\(data.vehicle_year_name ?? "")"
            self.colorLabel.text = "Color:\(data.vehicle_color_name ?? "")"
            self.makeLabel.text = "Make:\(data.vehicle_make_name ?? "")"
            self.modelLabel.text = "Model:\(data.vehicle_model_name ?? "")"
            self.plateLabel.text = "Plate #:\(data.license_plate_no ?? "")"
        }
        
        if auctionObj?.payment == 0{
            waitingForPaymentLabel.isHidden = false
            switchConfirmButton.isHidden = true
        }else{
            waitingForPaymentLabel.isHidden = true
            switchConfirmButton.isHidden = false
        }
        
//        if auctionObj?.payment == 0{
//            waitingForPaymentLabel.isHidden = true
//            switchConfirmButton.isHidden = false
//        }else{
//            waitingForPaymentLabel.isHidden = false       switchConfirmButton.isHidden = true
//        }
    }
    
    @objc func paymentDone(notfication: NSNotification) {
        waitingForPaymentLabel.isHidden = true
        switchConfirmButton.isHidden = false
    }
    
    @IBAction func onOpenMap(_ sender: UIButton) {
        
        //        let currantLocation = "\(currentLatitude)" + "," + "\(currentLongitude)"
        //        let actionLocation = "\(vahicaleDetail?.latitude ?? "")" + "," + "\(vahicaleDetail?.longitude ?? "")"
        //        let directionsURL = "http://maps.apple.com/?saddr=\(currantLocation)&daddr=\(actionLocation)"
        //        guard let url = URL(string: directionsURL) else {
        //            return
        //        }
        //        if #available(iOS 10.0, *) {
        //            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        //        } else {
        //            UIApplication.shared.openURL(url)
        //        }
        
        let latituide = ((self.vahicaleDetail?.latitude ?? "0.0") as NSString).doubleValue
        let longitude = ((self.vahicaleDetail?.longitude ?? "0.0") as NSString).doubleValue
        
        let coordinate = CLLocationCoordinate2DMake(latituide,longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        mapItem.name = "Parking Spot"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    
    @IBAction func onSwichConfirmed(_ sender: UIButton) {
        actionTransferPayment()
    }
    
    //MARK:-  Action Vehicl Detail API
    func actionVehicleDetail(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            let url = AUCTION_VEHICLE_DETAIL + "\(auctionObj?.auctions_id ?? 0)"
            AuctionServices.shared.actionDetail(URLString: url){ (statusCode, response) in
//                Utility.hideIndicator()
                print(response.auctionVehicleDetailsResponse?.toJSON())
                
                if let data = response.auctionVehicleDetailsResponse{
                    self.vahicaleDetail = data
                    self.setData()
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:-  Swich to confirm 
    func actionTransferPayment(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = imSpotRequest(auction_id: "\(auctionObj?.auctions_id ?? 0)")
            AuctionServices.shared.switchConfirmSeller(parameters:data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                self.openSwichSucess()
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }

            }else{
//            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func openSwichSucess(){
        let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.message = "Congratulations ! You completed your auction. You are now being redirected back to the homepage."
        vc.onClose = { [weak self] in
            self?.navigateSearchScreen()
        }
        self.present(vc, animated: true, completion: nil)
    }
    func navigateSearchScreen(){
        for controller in self.navigationController!.viewControllers as Array {
              if controller is AuctionScreen {
                  self.navigationController!.popToViewController(controller, animated: false)
                  break
              }
          }
    }
}
