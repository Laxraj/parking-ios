//
//  AuctionLiveScreen.swift
//  Val-A
//
//  Created by Nikunj on 14/03/21.
//

import UIKit

class AuctionLiveScreen: UIViewController {
    
    var onClose : (() -> Void)?
    @IBOutlet weak var detailLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        detailLabel.text = "Please check back periodically to see if your auction has any activity. You will receive a notification when your auction has been sold. Along with details on who you will be trading with as well as the exact location."
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onOk(_ sender: Any) {
        self.dismiss(animated: false, completion: { [weak self] in
            self?.onClose?()
        })
    }
}
