//
//  ConfirmAuctionScreen.swift
//  Val-A
//
//  Created by Nikunj on 14/03/21.
//

import UIKit

class ConfirmAuctionScreen: UIViewController {
    
    var auctionObj: AuctionResponse?
    
    var onEndAuctions: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onKeepAuction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onEndAuction(_ sender: Any) {
        self.endAuction()
    }
    
    //MARK:-  END AUCTION TIMER
    func endAuction(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuctionServices.shared.endAuction { (statusCode, respone) in
                Utility.hideIndicator()
                self.dismiss(animated: false, completion: { [weak self] in
                    self?.onEndAuctions?()
                })
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
