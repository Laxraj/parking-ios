//
//  AuctionScreen.swift
//  Val-A
//
//  Created by iroid on 20/02/21.
//

import UIKit
import MapKit
import DropDown
class AuctionScreen: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locationDescriptionTextField: dateSportTextField!
    @IBOutlet weak var timeOfExchangeTextField: UITextField!
    @IBOutlet weak var startBidTextField: dateSportTextField!
    @IBOutlet weak var freeOrPaidTextField: UITextField!
    @IBOutlet weak var feeTextField: dateSportTextField!
    @IBOutlet weak var sellButton: UIButton!
    @IBOutlet weak var zoneLocationTextField: dateSportTextField!
    
    var parkingOprtionArray = ["Free","Meter"]
    var freePaid = 1
    let datePicker = UIDatePicker()
    var zoneLocationArray: [ZoneListResponse] = []
    var zoneId = "0"
    var latitute = ""
    var logitute = ""
    var stripStatus = 0
    let locationManager = CLLocationManager()
    var isEnableLocationPermission: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.zoneLocationTextField.text = self.zoneId == "0" ? nil : self.zoneId
        feeTextField.isUserInteractionEnabled = true
        mapView.delegate = self
        latitute = currentLatitude
        logitute = currentLongitude
        setUpCurrantLocation()
        self.zoneLocationTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(yourfunction(notfication:)), name: NSNotification.Name(rawValue: "NEW_BID"), object: nil)
        getAccoutSetUpAPI()
        initialDetail()
    }
    
   
    
    func initialDetail(){
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
      
    }
    @objc func yourfunction(notfication: NSNotification) {
        self.getAuctionTimer()
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let txt = textField.text else {
            return
        }
        if let zone = self.checkZone(zone: txt){
            self.zoneId = zone
        }else{
            self.zoneId = "0"
        }
//      //  self.changePayButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.zoneLocationTextField.isUserInteractionEnabled = self.zoneId != "0" ? false : true
        self.setDatePicker()
        self.getZoneLocationListAPI()
        self.getAuctionTimer()
        self.checkLocationPermission()
        //self.changePayButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.clearAllTextField()
    }
    
    func checkLocationPermission(){
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                print("notDetermined")
            case .restricted:
                print("restricted")
            case .denied:
                print("denied")
                isEnableLocationPermission = true
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                isEnableLocationPermission = true
            @unknown default:
                print("lax")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func clearAllTextField(){
        self.zoneId = "0"
        self.zoneLocationTextField.text = nil
        self.locationDescriptionTextField.text = nil
        self.timeOfExchangeTextField.text = nil
        self.startBidTextField.text = nil
        self.freeOrPaidTextField.text = nil
        self.feeTextField.text = nil
        self.zoneLocationTextField.text = nil
    }
    
    func changePayButton(){
        if self.zoneId == "0"{
            self.sellButton.backgroundColor = .darkGray
            self.sellButton.isUserInteractionEnabled = false
        }else{
            self.sellButton.backgroundColor = Utility.getUIcolorfromHex(hex: "0A2250") 
            self.sellButton.isUserInteractionEnabled = true
        }
    }
    
    func checkZone(zone: String) -> String?{
        var zoneArr: [String] = []
        for i in self.zoneLocationArray{
            if let id = i.id{
                zoneArr.append(id)
            }
        }
        if let zone = zoneArr.first(where: {$0 == zone}){
            return zone
        }
        return nil
    }
    
    func getCureentHour() -> String{
        let dateFormattter = DateFormatter()
        dateFormattter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormattter.dateFormat = "HH"
        let hour = dateFormattter.string(from: Date())
        return hour
    }
    
    func setDatePicker() {
        //Format Date
        self.datePicker.datePickerMode = .dateAndTime
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        var components = DateComponents()
        components.year = 0
        components.minute = +32
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        self.datePicker.minimumDate = minDate
        
        var maxcomponents = DateComponents()
        components.year = 0
        maxcomponents.hour = (Int(self.getCureentHour()) ?? 0) + 12
        let maxDate = Calendar.current.date(byAdding: maxcomponents, to: Date())
        self.datePicker.maximumDate = maxDate
        
        self.datePicker.setDate(minDate ?? Date(), animated: true)


        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

        self.timeOfExchangeTextField.inputAccessoryView = toolbar
        self.timeOfExchangeTextField.inputView = self.datePicker
    }

    @objc func doneDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.timeOfExchangeTextField.text = formatter.string(from: self.datePicker.date)
        self.view.endEditing(true)
    }
    
    func getUTCDateFromTimePicker() -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter.string(from: self.datePicker.date)
    }

    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    //MARK:-  Zone Location API
    func getZoneLocationListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            PayParkService.shared.getZoneList{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.zoneLocationArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:-  GET AUCTION TIMER
    func getAuctionTimer(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            AuctionServices.shared.getAuctionTimer(success: { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    if res.is_timmer == 1{
                        self?.goFurther(obj: res)
                    }else{
                        if res.exchange_sports ?? false || res.payment == 1{
                            self?.exchangeSports(obj: res)
                        }else if res.place_bid ?? false{
                            self?.goFurther(obj: res)
                        }
                    }
                }
            }, failure: { [weak self] (error) in
//                guard let stronSelf = self else { return }
//                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
            })
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func goFurther(obj: AuctionResponse){
        let control = STORYBOARD.auction.instantiateViewController(withIdentifier: "CreateAnAuctionDetailScreen") as! CreateAnAuctionDetailScreen
        control.auctionObj = obj
        control.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(control, animated: false)
    }
    
    func exchangeSports(obj: AuctionResponse){
        let control = STORYBOARD.auction.instantiateViewController(withIdentifier: "ExchangeSpotsScreen") as! ExchangeSpotsScreen
        control.auctionObj = obj
        self.navigationController?.pushViewController(control, animated: false)
    }

//    func setUpCurrantLocation(){
//        let latDelta:CLLocationDegrees = 0.01
//
//        let longDelta:CLLocationDegrees = 0.01
//
//        let theSpan:MKCoordinateSpan =  MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
//        let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(currentLatitude) ?? 0, Double(currentLongitude) ?? 0)
//
//        let region:MKCoordinateRegion = MKCoordinateRegion(center: pointLocation, span: theSpan)
//        mapView.setRegion(region, animated: true)
//
//        let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(currentLatitude)!, Double(currentLongitude)!)
//
//        let objectAnnotation = MKPointAnnotation()
//        objectAnnotation.coordinate = pinLocation
//
//
//    }
    
//    // MARK: center Map On UserLocation
//    func setUpCurrantLocation(){
//        let latDelta:CLLocationDegrees = 0.10
//        let longDelta:CLLocationDegrees = 0.10
//        let theSpan:MKCoordinateSpan =  MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
//        let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(latitute) ?? 0, Double(logitute) ?? 0)
//        let region:MKCoordinateRegion = MKCoordinateRegion(center: pointLocation, span: theSpan)
//        mapView.setRegion(region, animated: true)
//        let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(latitute) ?? 0, Double(logitute) ?? 0)
//        let objectAnnotation = MKPointAnnotation()
//        objectAnnotation.coordinate = pinLocation
//        let pin = customPin(pinTitle:  "", Location: pinLocation, storeImage: "map_pin", actionId:0)
//        self.mapView.addAnnotation(pin)
//    }
    
    
    // MARK: center Map On UserLocation
    func setUpCurrantLocation(){
        let latDelta:CLLocationDegrees = 0.10
        let longDelta:CLLocationDegrees = 0.10
        let theSpan:MKCoordinateSpan =  MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        let pointLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(latitute) ?? 0, Double(logitute) ?? 0)
        let region:MKCoordinateRegion = MKCoordinateRegion(center: pointLocation, span: theSpan)
        self.mapView.showsUserLocation = true
        self.mapView.showsCompass = true
        self.mapView.setRegion(region, animated: true)
//        self.mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    @IBAction func onSell(_ sender: UIButton) {
        
//        if isEnableLocationPermission{
//            self.openLocationAlert()
//            return
//        }
        if stripStatus == 1{
            if let res = Utility.getUserInfoData(){
                if res.card_number == ""{
                    Utility.showAlert(vc: self, message: "Please enter card number in your profile")
                    return
                }
                if let validation = checkValidation(){
                    Utility.showAlert(vc: self, message: validation)
                }else{
                    self.sellAuctionAPI()
                }
            }
        }else{
            Utility.showAlert(vc: self, message: "Go to the Profile tab and click on \("Add Bank") to verify your bank account to begin using this service")
        }
    }
    
    @IBAction func onFreeOrPaid(_ sender: UIButton) {
        displayDropDown(array: parkingOprtionArray, tag: 0, view: self.freeOrPaidTextField)
    }
    
    func checkValidation() -> String?{
        if self.locationDescriptionTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter description"
        }else if self.timeOfExchangeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select time"
        }else if startBidTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter starting bid"
        }else if freeOrPaidTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select free or paid parking"
        }else if feeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select free or paid parking"
        }
//        else if zoneLocationTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
//            return "Please enter zone location"
//        }
        return nil
    }
    
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
             self?.freePaid = index == 0 ? 1 : 2
                self?.setFees(type: index)
                self?.freeOrPaidTextField.text = item
                dropDown.hide()
            default:
                break
               
            }
        }
    }
    func setFees(type:Int){
        if type == 1{
            feeTextField.isUserInteractionEnabled = true
            feeTextField.text = "0"
        }else{
            feeTextField.isUserInteractionEnabled = false
            feeTextField.text = "0"
        }
    }
    func sellAuctionAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            let data = AuctionSellRequest(description: (self.locationDescriptionTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""), time_of_exchange: self.getUTCDateFromTimePicker(), startbid: (self.startBidTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""), free_paid: "\(self.freePaid)", fee: self.freePaid == 1 ? nil : (self.feeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""), zone_id: self.zoneId, longitude: currentLongitude, latitude: currentLatitude)
            AuctionServices.shared.CreateAnAuction(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                if let data = response{
                    print(data.toJSON())
                    self.openAuctionLive(obj: data)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getAccoutSetUpAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndicator()
            LoginService.shared.getAccountStatus(parameters: [:]) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.stripStatusResponse{
                    self?.stripStatus = res.stripe_status ?? 0
                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func openAuctionLive(obj: AuctionResponse){
        let vc = STORYBOARD.auction.instantiateViewController(withIdentifier: "AuctionLiveScreen") as! AuctionLiveScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.onClose = { [weak self] in
            self?.goFurther(obj: obj)
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func goFurther(object: PayForParkResponse){
        
        func openSucess(){
            let vc = STORYBOARD.profile.instantiateViewController(withIdentifier: "SuccessScreen") as! SuccessScreen
            vc.modalPresentationStyle = .overCurrentContext
            vc.message = "Please check back periodically to see if your auction has any activity. You will receive a notification when your auction has been sold. Along with details on who you will be trading with as well as the exact location."
            vc.onClose = { [weak self] in
                let control = STORYBOARD.auction.instantiateViewController(withIdentifier: "CreateAnAuctionDetailScreen") as! CreateAnAuctionDetailScreen
                self?.navigationController?.pushViewController(control, animated: true)
            }
            self.present(vc, animated: true, completion: nil)
        }
        
//        let control = STORYBOARD.payForParkDescription.instantiateViewController(withIdentifier: "payForParkDescriptionScreen") as! payForParkDescriptionScreen
//        control.obj = object
//        self.navigationController?.pushViewController(control, animated: true)
    }
}
extension AuctionScreen:MKMapViewDelegate{
    // MARK: Set Pin On MapView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            annotationView?.canShowCallout = true
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
//            pin.image = UIImage(named: "map_pin")
            
            pin.isEnabled = true
            pin.canShowCallout = true
            let storeImageView = UIImageView()
            storeImageView.frame = CGRect(x: -25, y: -25, width: 34, height: 42)
            
            if annotation is customPin{
                let cpa = annotation as? customPin
                storeImageView.image = UIImage(named: cpa?.storeImage ?? "")
                pin.addSubview(storeImageView)
                annotationView = pin
            }
            
        }
        return annotationView
    }
}
extension AuctionScreen : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
           
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            
            break
        case .authorizedAlways:
            // If always authorized
            ()
            manager.startUpdatingLocation()
            break
        case .restricted:
            self.openLocationAlert()
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            self.openLocationAlert()
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.openLocationAlert()
    }
    
    func openLocationAlert(){
        let alert = UIAlertController(title: "Settings", message: "Allow location permission from settings", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Open", style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
