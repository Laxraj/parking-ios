//
//  AuctionCollectionViewCell.swift
//  Val-A
//
//  Created by iroid on 11/03/21.
//

import UIKit

class AuctionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var zoneNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var actionView: UIView!
    
    var selectedIndex = -1
    var releaseDate: Date?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUpData(model:AuctionResponse,indexPath:Int){
        if selectedIndex == indexPath{
            actionView.layer.borderColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        }else{
            actionView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
       
        
        if model.menu_type == 1{
            let price = (model.highest_bid == "0" || model.highest_bid == "") ? "\(model.startbid ?? "0")" : "\(model.highest_bid ?? "0")"
            priceLabel.text = "$" + "\(price)"
            timeLabel.text = model.time_of_exchange
            if model.time_of_exchange != ""{
                startTimer(date: model.time_of_exchange ?? "")
            }
            timeLabel.isHidden = false
            zoneNameLabel.text = model.description
        }else{
            zoneNameLabel.text = model.zone_name
            priceLabel.text = model.availability
            timeLabel.isHidden = true
        }
        
        
    }
    
    
    func startTimer(date:String) {

        let releaseDateString = date
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        releaseDate = releaseDateFormatter.date(from: releaseDateString)! as Date
        
        
        let currentDate = Date()
        let calendar = Calendar.current

        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: releaseDate! as Date)

        let countdown = "Days \(diffDateComponents.day ?? 0), Hours \(diffDateComponents.hour ?? 0), Minutes \(diffDateComponents.minute ?? 0), Seconds \(diffDateComponents.second ?? 0)"
        
        self.timeLabel.text = String(format: "%02d:%02d:%02d", diffDateComponents.hour ?? 0, diffDateComponents.minute ?? 0, diffDateComponents.second ?? 0)

        print(countdown)
       
    }
}
