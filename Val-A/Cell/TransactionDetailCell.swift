//
//  TransactionDetailCell.swift
//  Val-A
//
//  Created by Nikunj on 24/02/21.
//

import UIKit

class TransactionDetailCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var zoneLabel: UILabel!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: TransactionHistoryResponse?{
        didSet{
            self.dateLabel.text = item?.date
            self.zoneLabel.text = item?.zone
            self.transactionTypeLabel.text = item?.transaction_type
            self.priceLabel.text = item?.price
            self.paymentTypeLabel.text = item?.payment_type
            self.modelLabel.text = item?.vehicle_model_name
        }
    }
    
}
