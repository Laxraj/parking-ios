//
//  OnBoardCell.swift
//  Val-A
//
//  Created by Nikunj on 02/04/21.
//

import UIKit

class OnBoardCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var item: OnBoardRequest?{
        didSet{
            self.imageView.image = item?.image
            self.label.text = item?.description
        }
    }

}
