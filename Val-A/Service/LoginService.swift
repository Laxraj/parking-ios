//
//  LoginService.swift
//  SportCoach
//
//  Created by iroid on 06/01/21.
//

import Foundation
class LoginService {
    static let shared = { LoginService() }()
    
    //MARK:- LOGIN
    func login(parameters: [String: Any] = [:],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: loginURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Vehicle Make List URL
    func getVehicleList(success: @escaping (Int, [VehicleMakeResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleMakeURL) { (statusCode, response) in
            success(statusCode,response.vehicleMakeResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Vehicle Model List URL
    func getVehicleModelList(vehicleMakeId: Int,success: @escaping (Int, [VehicleModelResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleModelURL+"/\(vehicleMakeId)") { (statusCode, response) in
            success(statusCode,response.vehicleModelResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Vehicle Year List URL
    func getVehicleYearList(success: @escaping (Int, [VehicleYearResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleYearURL) { (statusCode, response) in
            success(statusCode,response.vehicleYearResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Vehicle Color List URL
    func getVehicleColorList(success: @escaping (Int, [VehicleColorResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleColorURL) { (statusCode, response) in
            success(statusCode,response.vehicleColorResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- REGISTER USER
//    func registerUser(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
//        APIClient.shared.requestAPIWithParameters(method: .post, urlString: registerURL, parameters: parameters) { (statusCode, response) in
//            success(statusCode,response)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
//
    func registerUser(parameters: [String: Any],image: Data?,success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestWithImage(urlString: registerURL, imageParameterName: "profile_pic", images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        } 
    }
    
    
    //MARK:- Register for push
    func registerForPush(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: registerFotPusgURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- FORGOT PASSWORD USER
    func resetPassword(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: resetPasswordURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- SOCIAL LOGIN
    func socialLogin(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: socialLoginURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Social Login is First Time
    func isFirstTime(userID: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: isLoginFirstTimeUrl+"/\(userID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- get user info
    func getUserInfo(success: @escaping (Int, UserInfoResponse?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: getUserInfoURL) { (statusCode, response) in
            success(statusCode,response.userInfoResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    
    func getAccountStatus(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: accountStatusURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK:- get user info
    func getAccountStatus(success: @escaping (Int, UserInfoResponse?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: getUserInfoURL) { (statusCode, response) in
            success(statusCode,response.userInfoResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- Update user info
    func updateUserInfo(parameters: [String: Any],image: Data?,success: @escaping (Int, UserInfoResponse?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestWithImage(urlString: updateUserInfoURL, imageParameterName: "profile_pic", images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.userInfoResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Logout
    func logout(success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: logoutURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK: - DELETE ACCOUNT
    func deleteAccount(success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: deleteAccountURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- APP VERSION
    func appVerion(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: appVersion, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Strip account create
    func createStripAccount(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: stripAccountCreateURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CHANGE PASSWORD
    func changePassword(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: changePasswordURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - WITHDRAW AMOUNT
    func WithdrawAmount(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: withdrawAmountURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
}
