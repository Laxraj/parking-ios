//
//  PayParkService.swift
//  Val-A
//
//  Created by Nikunj on 06/03/21.
//

import Foundation
class PayParkService {
    static let shared = { PayParkService() }()
    
    //MARK:- Zone  List URL
    func getZoneList(success: @escaping (Int, [ZoneListResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: zoneListURL) { (statusCode, response) in
            success(statusCode,response.zoneListResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- Parking Time Duration List URL
    func getParkingTimeDurationList(success: @escaping (Int, [ParkingTimeDurationResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: parkingTimeDuration) { (statusCode, response) in
            success(statusCode,response.parkingTimeDurationResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- PAY FOR PARK
    func payForPark(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: payForParkURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- GET TIMER
    func getTimer(success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: getTimerURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- GET TIMER
    func endTimer(success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: endTimerURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- EXTENDED MORE TIME
    func extendMoreTime(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: extendedTimeURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- GET TRANSACTION HISTORY
    func getTransaction(success: @escaping (Int, [TransactionHistoryResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: transactionHistoryURL) { (statusCode, response) in
            success(statusCode,response.transactionHistoryResponse)
        } failure: { (error) in
            failure(error)
        }
    }
}
