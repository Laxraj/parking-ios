//
//  AuctionServices.swift
//  Val-A
//
//  Created by iroid on 14/03/21.
//

import Foundation
class AuctionServices {
    static let shared = { AuctionServices() }()
    
   
    
    //MARK:- Create An Auction
    func CreateAnAuction(parameters: [String: Any] = [:],success: @escaping (Int, AuctionResponse?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: createAuction, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.getAuctionResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Parking Time Duration List URL
//    func getAuctionList(success: @escaping (Int, [AuctionResponse]?) -> (), failure: @escaping (String) -> ()) {
//        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: getAuctionURL) { (statusCode, response) in
//            success(statusCode,response.auctionResponse)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
    
    func getAuctionList(parameters: [String: Any] = [:],success: @escaping (Int,[AuctionResponse]?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: getAuctionURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.auctionResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- GET AUCTION TIMER
    func getAuctionTimer(success: @escaping (Int, AuctionResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: auctionTimerURL) { (statusCode, response) in
            success(statusCode,response.getAuctionResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- END AUCTION
    func endAuction(success: @escaping (Int, AuctionResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: endAuctionURL) { (statusCode, response) in
            success(statusCode,response.getAuctionResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- PLACE BID
    func placeBid(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: placeBidURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    func actionDetail(URLString:String,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString:  URLString) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Parking Time Duration List URL
    func getAuctionTimeDurationList(success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: GET_BUYER_TIMMER) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK:- Client Commission URL
    func clientCommission(success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: commissionUrl) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Cancel bid 
    func cancelPlaceBid(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: DELETE_PLACE_BID, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func imSpot(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: I_M_SPOT_URL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    func actionTransferPayments(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: transferPaymentURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func switchConfirmSeller(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: switchConfirmedURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func switchBuyerConfirmSeller(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: switchBuyerConfirmedURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
   
    
    
    //MARK:- Cancel spot
    func cancelSpot(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: CANCEL_SPOT, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
  
}
